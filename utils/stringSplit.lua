--[[

	strToTable(): Basically the equivalent of an explode() or split() function.  Takes a
		string and a separator, and uses that to split a single string into an indexed
		table.

		As a note, it will automatically convert to numeric formats when available.

]]--
function strToTable(aStr,sep,rev)

	if(type(aStr) ~= 'string') then return nil end
	local mySep = sep or '%s'
	if(string.len(mySep) == 0) then mySep = ',' end	
	local myPat = '([^'..mySep..']+)'..mySep..'?'
	local nxStart = 0
	local nxEnd = -1
	local nxElement = true
	local retArray = {}

	if not rev then
		while nxStart ~= nil do
			nxStart, nxEnd, nxElement = string.find(aStr, myPat, nxEnd + 1)
			retArray[#retArray + 1] = tonumber(nxElement) or nxElement
		end
	else
		local indPat = '([^'..mySep..']+)'
		local revPat = indPat .. '$'
		while nxStart ~= nil do
			print('Search pattern: "'..revPat..'"')
			nxStart, nxEnd, nxElement = string.find(aStr, revPat)
			retArray[#retArray + 1] = tonumber(nxElement) or nxElement
			revPat = indPat .. mySep .. revPat
		end
	end

	return retArray

end

--[[

	strSplit(): This should do the same as the above, but preferably without tables.
		(e.g. Uses recursion, and returns a list of values instead of a table.)

		Pro: Less overhead (no table creation, etc.)
		Con: Lists are less flexible.

]]--

function strSplit(aStr, sep, aStart)

	if(type(aStr) ~= 'string') then return nil end
	local mySep = sep or '%s'
	if(string.len(mySep) == 0) then mySep = '%s' end	
	local myPat = '([^'..mySep..']+)'..mySep..'?'
	local myStart = aStart or 0
	local nxStart, nxEnd, myElement = string.find(aStr, myPat, aStart)

	local myRet = tonumber(myElement) or myElement

	if(myRet) then
		return myRet, strSplit(aStr, sep, nxEnd + 1)
	else
		return myRet
	end

end

function strSplitRev(aStr, sep, pat)

	if(type(aStr) ~= 'string') then return nil end
	local mySep = sep or '%s'
	if(string.len(mySep) == 0) then mySep = '%s' end	
	local indPat = '([^'..mySep..']+)'
	local myPat = pat or '$'
	local revPat = indPat .. myPat
	local myStart = aStart or 0
	local nxStart, nxEnd, myElement = string.find(aStr, revPat, aStart)

	local myRet = tonumber(myElement) or myElement

	if(myRet) then
		return myRet, strSplitRev(aStr, sep, mySep..revPat)
	else
		return myRet
	end

end

--[[	--TEST CODE
myStr = 'hero.STAND.1'

myTab = strToTable(myStr,'%.',true)

print('Running strToArray on "'..myStr..'"')

for k,v in ipairs(myTab) do
	print(k..'('..type(v)..')'..'='..v)
end

print('\nRunning strSplitRev on "'..myStr..'"')

local x,y,z = strSplitRev(myStr,'%.')

print('x='..x)
print('y='..y)
print('z='..z)
]]--
