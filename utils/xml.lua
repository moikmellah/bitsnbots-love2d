--[[

	xml.lua - Provides xml parsing functionality.

	Adapted from code by Roberto Ierusalimschy; see http://lua-users.org/wiki/LuaXml
		for original source.

	This code is freely distributable under the zLib license.

--]]

function xmlParseArgs(s)
  local arg = {}
  string.gsub(s, "(%w+)=([\"'])(.-)%2", function (w, _, a)
    arg[w] = a
  end)
  return arg
end
    
function xmlCollect(s)
  local stack = {}
  local top = {}
  table.insert(stack, top)
  local ni,c,label,xarg, empty
  local i, j = 1, 1
  while true do
    ni,j,c,label,xarg, empty = string.find(s, "<(%/?)([%w:]+)(.-)(%/?)>", i)
    if not ni then break end
    local text = string.sub(s, i, ni-1)
    if not string.find(text, "^%s*$") then
      table.insert(top, text)
    end
    if empty == "/" then  -- empty element tag
      table.insert(top, {label=label, xarg=xmlParseArgs(xarg), empty=1})
    elseif c == "" then   -- start tag
      top = {label=label, xarg=xmlParseArgs(xarg)}
      table.insert(stack, top)   -- new level
    else  -- end tag
      local toclose = table.remove(stack)  -- remove top
      top = stack[#stack]
      if #stack < 1 then
        error("nothing to close with "..label)
      end
      if toclose.label ~= label then
        error("trying to close "..toclose.label.." with "..label)
      end
      table.insert(top, toclose)
    end
    i = j+1
  end
  local text = string.sub(s, i)
  if not string.find(text, "^%s*$") then
    table.insert(stack[#stack], text)
  end
  if #stack > 1 then
    error("unclosed "..stack[#stack].label)
  end
  return stack[1]
end

function xmlLoadFile(xmlPath)

	-- Just return if no path given.
	if xmlPath == nil then return nil end
--[[
	-- Attempt to open; return nil on failure.
	local fileHandle = io.open(xmlPath, 'r')
	if fileHandle == nil then return nil end

	-- Attempt to read; return nil on failure.
	local xmlStr = fileHandle:read('*a')
]]--

	xmlStr = love.filesystem.read(xmlPath)

	if xmlStr == nil then return nil end

	local xmlObj = xmlCollect(xmlStr)

	return xmlObj

end

