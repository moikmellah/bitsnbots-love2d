--[[
Copyright 2011 MoikMellah

This file is part of Bits & Bots.

Bits & Bots is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
Bits & Bots. If not, see http://www.gnu.org/licenses/
]]--

--[[

	utils.lua - miscellaneous utility functions for global usage

--]]

-- tableCopy(a) - recursively returns a full copy of table 'a' (minus threads)
function tableCopy(a)

	local b = nil

	if type(a) == "table" then

		b = {}

		for k,v in pairs(a) do

			if type(v) == "table" then
				b[k] = tableCopy(a[k])
			elseif type(v) ~= "thread" then
				b[k] = a[k]
			end

		end

	end

	return b

end



--[[

	objDepthComp(obj a, obj b) - Compares relative depth of two objects.  Used for table sort.

	Question: Are 'nil' values even compared?  A: They shouldn't be.  Remove obsolete objs with table.remove().

]]--

function objDepthComp(a,b)

	-- First: fetch camera values and compare (no need to go further than that, if they differ.)
	local aCam = a.cam
	local bCam = b.cam
	if aCam == nil then aCam = 0 end
	if bCam == nil then bCam = 0 end
	if aCam > bCam then return true end
	if bCam > aCam then return false end

	-- Next: capture depth values and compare.
	local aDep = a.depth
	local bDep = b.depth
	if aDep == nil then aDep = 0 end
	if bDep == nil then bDep = 0 end
	if aDep > bDep then return true end
	if bDep > aDep then return false end

	-- Still equal?  Try y values, then x values
	local ay = a.y
	local by = b.y
	if ay < by then return true end
	if by < ay then return false end
	local ax = a.x
	local bx = b.x
	if ax > bx then return true end
	if bx > ax then return false end

	-- STILL equal?  Use the index from objs array, for stability - hopefully kill flickering
	local ai = a.index
	local bi = b.index
	if ai < bi then return true end

	-- All vals equal?  Just return false.  WARNING: sprites with all-equal values, at this point, may cause flicker.
	return false

end

function nothing()

	while 1 == 1 do
		coroutine.yield()
	end

end

function fileToString(file)

	local str = ""

--	local fh = io.open(file)
--	str = str..fh:read("*a")
--	fh:close()

	str = love.filesystem.read(file)

	return str

end

function pixels(n)

	return n * pixelsPerUnit

end
