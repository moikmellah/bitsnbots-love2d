if(love._version_minor == 9) then
	love.graphics.drawq = love.graphics.draw
	love.graphics.quad = love.graphics.polygon
	love.graphics.setDefaultImageFilter = love.graphics.setDefaultFilter
	love.graphics.toggleFullscreen = function () 
		local isFS = love.window.getFullscreen() 
		love.window.setFullscreen(not(isFS)) 
		local newWidth,newHeight = love.window.getDimensions()
		if (not(isFS) and ((newHeight / newWidth) < .75)) then
			winOfs = (newWidth - (newHeight * 4 / 3)) / (love.graphics.getHeight() / 240 * 2)
			--print(winOfs..'('..newWidth..';'..newHeight..')')
		else
			winOfs = 0
		end
	end
	love.v = 9
elseif(love._version_minor == 8) then
	love.window = love.graphics
	love.graphics.setDefaultFilter = love.graphics.setDefaultImageFilter
	love.system = {}
	love.system.getOS = function() return love._os end
	math.randomseed(os.time())
	love.math = {random = math.random}
	love.v = 8
end 
