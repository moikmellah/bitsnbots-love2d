--[[
Copyright 2014 MoikMellah

This file is part of Project SYPHA.

Project SYPHA is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
Project SYPHA. If not, see http://www.gnu.org/licenses/
]]--

--[[

	globals.lua - definitions of global static values

]]--


-- Keys, mouse buttons, etc.
INPUT_DU = 0x8000	-- Up Arrow
INPUT_DD = 0x4000	-- Down Arrow
INPUT_DL = 0x2000	-- Left Arrow
INPUT_DR = 0x1000	-- Right Arrow

INPUT_BA = 0x0800	-- s
INPUT_BB = 0x0400	-- Space bar
INPUT_BX = 0x0200	-- a
INPUT_BY = 0x0100	-- d
INPUT_BL = 0x0080	-- f
INPUT_BR = 0x0040	-- Left shift
INPUT_BSEL = 0x0020	-- Enter key
INPUT_BST = 0x0010	-- Esc key

INPUT_ML = 0x0008	-- Left mouse btn
INPUT_MR = 0x0004	-- Right mouse btn

vpWidth = 320
vpHeight = 240

pixelsPerUnit = 8

speedAdjust = 32

-- Active GameObject list; contains both GameObjects and BkgObjects
objs = nil

screenfx = {r=1,g=1,b=1,a=1,max=false}

-- cameras - contains references to active BkgObjects
cams = nil

activeFont = nil

fontLib = {}

-- maps - contains templates, load/unload functions, etc.
maps = {}

--mapLib - simple list of MapObjects
mapLib = {}

-- toKill - table of objs what need killin'
toKill = {}

dropct = 0
globalCtx = nil
bq = nil
scrMirror = {x=1,y=1}

fadeColor = nil

-- Scoring table
rankTable = {
	[1] = {min = 0, max = 1000, rank = "CHEATER!!"},
	[2] = {min = 1001, max = 2000, rank = "Godlike!"},
	[3] = {min = 2001, max = 2500, rank = "Masterful!"},
	[4] = {min = 2501, max = 3000, rank = "Brilliant!"},
	[5] = {min = 3001, max = 3500, rank = "Amazing!"},
	[6] = {min = 3501, max = 4000, rank = "Excellent!"},
	[7] = {min = 4001, max = 4500, rank = "Great!"},
	[8] = {min = 4501, max = 5000, rank = "Good!"},
	[9] = {min = 5001, max = 5500, rank = "Not bad."},
	[10] = {min = 5501, max = 6000, rank = "Okay."},
	[11] = {min = 6001, max = 6500, rank = "Decent."},
	[12] = {min = 6501, max = 7000, rank = "Average."},
	[13] = {min = 7001, max = 7500, rank = "Meh."},
	[14] = {min = 7501, max = 8000, rank = "Rough."},
	[15] = {min = 8001, max = 8500, rank = "Needs work."},
	[16] = {min = 8501, max = 9000, rank = "Keep practicing."},
	[17] = {min = 9001, max = 99999, rank = "Yikes. Try again."}
}

menuMusic = "music/menu-skrjablin.ogg"
gameMusic = "music/caveexplorer_hq.mp3"
