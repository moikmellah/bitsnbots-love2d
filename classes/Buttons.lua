--[[
Copyright 2014 MoikMellah

This file is part of Bits & Bots.

Project SYPHA is free software: you can redistribute it and/or modify it under the terms
of the ZLIB License.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the ZLIB License for more details.

You should have received a copy of the ZLIB License along with
Bits & Bots. If not, see http://zlib.net/zlib_license.html
]]--

Buttons = {}

--[[

waitToggle() - The handler for the Bit buttons.

]]--
Buttons.waitToggle = function (self,dtA)

	local dummy = nil
	local dt = dtA

	-- Initial state/position is 'off' and 'up'
	self.btnState = 0
	self.btnPos = 0

	-- Use for mouse interaction
	local tempX = 0
	local tempY = 0

	-- Store which position we're in (randomly determined at map init)
	self.btnBit = btnPos[self.btnId]

	-- Move to appropriate location
	self.x = (self.btnBit * 32) + 24

	local myCam = nil

	local msC = 0

	-- Main loop for bit button
	while 1 == 1 do

		-- Set image to appropriate value
		self.quad = (self.btnState * 2) + self.btnPos + 1

		-- Fetch mouse pos
		tempX,tempY = self:mousePos()

		if (self.debug == 1) then print('x:'..self.x..';q:'..self.quad..';v:'..self.visible) end

		-- Proceed if input is allowed
		if inputOk == true then

			-- Is the left mouse button down?
			if InputLibrary:down('mouseleft') then

				-- Is the mouse currently intersecting this button?
				if tempX > self.x - 12 and tempX < self.x + 12 and tempY > self.y - 12 and tempY < self.y + 12 then

					-- If the button is up, set it to down and toggle its state
					if self.btnPos == 0 then

						AudioLibrary:play('sfx/click.ogg')

						self.btnPos = 1
						self.btnState = (self.btnState + 1) % 2

					end

				-- Not intersecting - button should be up by default
				else self.btnPos = 0
				end

			-- No mouse event - button should be up by default
			else self.btnPos = 0
			end

			-- Update registry to reflect current on/off state
			btnVal[self.fct][self.val] = self.btnState
			btnVal.byPos[self.btnBit + 1] = self.btnState

		end

		dummy,dt = coroutine.yield()

		-- If all bots have reached their goal, go into the 'marquee scrolling' state
		if botWin[1] == true and botWin[2] == true and
		botWin[3] == true and botWin[4] == true then
			self.nextLogic = Buttons.winBlink
			return nil
		end

	end

end

-- waitExec - the Exec button

Buttons.waitExec = function (self,dtA)

	local dummy = nil
	local dt = dtA

	self.btnPos = 0

	local tempX = 0
	local tempY = 0

	local myCam = nil

	local msC = 0

	local winState = false

	local bc = 0
	local bcLen = 1

	while bc < 1 do
		bc = math.max(0,math.min(1,bc + (dt / bcLen)))
		screenfx.r = bc 
		screenfx.g = bc 
		screenfx.b = bc 
		AudioLibrary:setMaster(bc)
		dummy,dt = coroutine.yield()
	end

	while 1 == 1 do

		if InputLibrary:down('menu') and not(InputLibrary:ack('menu')) then

			InputLibrary:setAck('menu')

			bc = 1
			
			while bc > 0 do
				bc = math.max(0,math.min(1,bc - (dt / bcLen)))
				screenfx.r = bc 
				screenfx.g = bc 
				screenfx.b = bc 
				AudioLibrary:setMaster(bc)
				dummy,dt = coroutine.yield()
			end

			loadRequest = 2

		end

		self.quad = 5 + self.btnPos

		tempX,tempY = self:mousePos()

		if inputOk == true then

			if InputLibrary:down('mouseleft') then

				if tempX > self.x - 12 and tempX < self.x + 12 and tempY > self.y - 12 and tempY < self.y + 12 then

					if self.btnPos == 0 then

						AudioLibrary:play('sfx/boop.ogg')

						self.btnPos = 1
						self.quad = 6
						cmdGo = 1
						inputOk = false

						-- Begin ghetto hex conversion..
						local hexL = 0
						local hexR = 0

						for hexc = 1,4 do
							if btnVal.byPos[hexc] == 1 then hexL = hexL + (2 ^ (4 - hexc)) end
							if btnVal.byPos[hexc + 4] == 1 then hexR = hexR + (2 ^ (4 - hexc)) end
						end

						if hexL > 9 then hexL = btnVal.hex[hexL] end
						if hexR > 9 then hexR = btnVal.hex[hexR] end

						-- Spawn a label-only widget to display the hex value of the active buttons
						local widget = self:spawn(maps[1].templates[19])
						widget.label.text = "["..hexL..hexR.."]"
						widget.depth = self.depth - 1

						for bc = 1,4 do botCheck[bc] = botWin[bc] end

						-- Increment score.moves, ONLY if a bot is still in the field
						if (botCheck[1] == false or botCheck[2] == false 
						or botCheck[3] == false or botCheck[4] == false) and (hexL ~= 0 or hexR ~= 0) then
							score.moves = score.moves + 1
						end

						while botCheck[1] == false or botCheck[2] == false 
						or botCheck[3] == false or botCheck[4] == false do
							dummy,dt = coroutine.yield()
						end

						--self.btnPos = 0
						--self.quad = 4
						--self:spawn(maps[1].templates[5])
						cmdGo = 0
						inputOk = true
						-- fadeColor = nil
						for bc = 1,4 do botCheck[bc] = botWin[bc] end

					end

				else self.btnPos = 0
				end

			else self.btnPos = 0
			end

		end

		dummy,dt = coroutine.yield()

	end

end

--[[

	winBlink() - buttons blink in sequence (hopefully)

--]]

Buttons.winBlink = function(self,dtA)

	local dummy = nil
	local dt = dtA

	-- frame counter
	local fc = 0

	-- frame delay - adjust to speed up or slow down marquee effect
	local ft = .2

	-- blink delay - adjusts how close together the lights are (min 2; 1 is chaotic, and doesn't give a visible scrolling effect)
	local fd = 2

	self.quad = 1

	-- initial wait, to sequence the blinking
	while fc < (ft * self.btnBit) do
		fc = fc + dt
		dummy,dt = coroutine.yield()
	end

	-- Marquee control loop
	while 1 == 1 do

		-- Set button to 'on'
		self.quad = 3
		btnVal.byPos[self.btnBit + 1] = 1

		-- Remain in that state for <ft> cycles
		--for fc = 1,ft do dummy,dt = coroutine.yield() end
		fc = 0
		while fc < ft do 
			fc = fc + dt
			dummy,dt = coroutine.yield() 
		end

		-- Set button to 'off'
		self.quad = 1
		btnVal.byPos[self.btnBit + 1] = 0

		fc = 0

		-- Remain in that state for <ft>*<fd> cycles
		--for fc = 1,(ft*fd) do
		while fc < ft*fd do 
			fc = fc + dt
			dummy,dt = coroutine.yield() 
		end

	end

end



--[[

	Buttons.hexFloat - simple 'float upward' routine for label (should probably move this to Textbox)

]]--

Buttons.hexFloat = function(self,dtA)

	local dummy = nil
	local dt = dtA

	local myTimer = 0

	local limit = .68

	self.label.color = self.label.color or {r=1,g=1,b=1,a=1}

	-- Only go for X cycles
	while myTimer < limit do

		-- Increment timer, move upward, and fade out
		myTimer = myTimer + dt
		self.y = self.y - 32 * dt
		self.label.color.a = math.max(0,math.min(1 - (myTimer / limit),1))

		dummy,dt = coroutine.yield()

	end

	-- Done - release resources
	self.killme = true

end
