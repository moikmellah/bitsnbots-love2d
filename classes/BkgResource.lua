--[[
Copyright 2014 MoikMellah

This file is part of Bits & Bots.

Project SYPHA is free software: you can redistribute it and/or modify it under the terms
of the ZLIB License.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the ZLIB License for more details.

You should have received a copy of the ZLIB License along with
Bits & Bots. If not, see http://zlib.net/zlib_license.html
]]--

-- Member fields template
BkgResDefaults = {

	pitch = 0,			-- width (in tiles)
	height = 0,			-- height (in tiles)
	filepath = "resource/stupidTest.80x60.map", -- path to data file
	buf = nil,			-- lightuserdata reference to buffer
	isLoaded = false,		-- have we requested C to load the buffer from file yet?
	luaClass = "BkgResource",	-- Lua class name
	class = "BkgResource",		-- engine class name
	name = "Default Map"		-- object's name

}

BkgResource = {}

BkgResource_mt = {}
BkgResource_mt.__index = BkgResource

function BkgResource.new(template)

	print("BkgRes testing")

	local bTmp = nil
	if type(template) == table then
		bTmp = template
	else
		bTmp = BkgResDefaults
	end

	-- Make copy from template if provided, or default template if not
	local bObj = tableCopy(bTmp)

	-- Set metatable to get our class methods
	setmetatable(bObj, BkgResource_mt)

	-- Attempt to load the buffer from source file
	bObj:loadFile()

	return bObj

end

function BkgResource:toString()

	local str = ""

	if self.name ~= nil then str = self.name
	elseif self.class ~= nil then str = self.class
	elseif self.luaClass ~= nil then str = self.luaClass
	else str = "BkgResource" end

	return str

end

function BkgResource:loadFile()

	local fileStr = self.filepath

	local foundLast = false

	-- Loop through until we find only the filename
	while foundLast == false do
		local slashpos = string.find(fileStr, "/")
		if slashpos then
			fileStr = string.sub(fileStr, slashpos + 1)
		else
			foundLast = true
		end
	end

	-- Strip out the dimensions
	local dimstart = string.find(fileStr, "%.") + 1
	local dimend = string.find(fileStr, "%.", dimstart + 1) - 1

	local dimstr = string.sub(fileStr, dimstart, dimend)

	-- Find our 'x' delimiter
	local xpos = string.find(dimstr, "x")

	-- Set our dimensions from filename
	self.pitch = string.sub(dimstr, 1, xpos - 1)
	self.height = string.sub(dimstr, xpos + 1)

	self:cLoadBkgRes()

	-- if self.buf then print("Buffer loaded.") else print("Buffer load failed.") end

end


