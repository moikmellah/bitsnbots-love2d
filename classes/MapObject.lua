--[[
Copyright 2014 MoikMellah

This file is part of Bits & Bots.

Project SYPHA is free software: you can redistribute it and/or modify it under the terms
of the ZLIB License.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the ZLIB License for more details.

You should have received a copy of the ZLIB License along with
Bits & Bots. If not, see http://zlib.net/zlib_license.html
]]--

require('utils.xml')

-- Table declaration for class
MapObject = {}

-- Metatable for class (to make MapObject table the default lookup for class methods)
MapObject_mt = {}
MapObject_mt.__index = MapObject

-- Might deprecate the constructor in favor of a global class 'init()' method.

-- Constructor
function MapObject.new(mapPath)

	-- Make copy from template if provided, or default template if not
	local nMapLib = {}

	nMapLib.maps = {}
	nMapLib.activeMap = ''

--	nMapLib.images = ImageLibrary.new()

	-- Set metatable to get our class methods
	setmetatable(nMapLib, MapObject_mt)

	nMapLib:loadMap(mapPath)

	nMapLib.camx = 0
	nMapLib.camy = 0
	nMapLib.lzx = 0
	nMapLib.lzy = 0

	--[[
		Should we process things here, or in an overall Library class?
		..Library makes more sense, so we can have centralized storage
		of tilesets (e.g. local instance of ImageLibrary).
	]]--

	return nMapLib

end

--[[

	..Sigh.  Reorganizing again.  Todo:

	- Figure out how terrain should be handled (tile properties of main layer, or separate layer?)
		- Tile properties is cleaner, but we'd need additional properties to put the 'collision'
			attribute into each tile.  Then, when testing collisions, grab tiles[layer.data[pos]\].coll.
		- Separate layer would allow for e.g. invisible passages using the same tile as surroundings,
			but we'd still need to either track those gid's (ugh) or use properties as above.

	- Improve tileset processing.  If we use tile properties, we can't make the assumption that child
		element '1' of the tileset node is always the image source.  (Probably shouldn't anyway.)

	Basically, this is messy.  Fix it.

]]--

function MapObject:loadMap(mapName,x,y)

	local thisMap = self

	local xmlTable

	if (mapLib[mapName] == nil) then
		xmlTable = xmlLoadFile(mapName)
		if (xmlTable == nil) then return false end
	else
		return true
	end

--	thisMap = {}
	thisMap.layers = {}
	thisMap.width = 0
	thisMap.height = 0
	thisMap.tileWidth = 0
	thisMap.tileHeight = 0

	for k,v in pairs(xmlTable) do
		if((type(v) == 'table') and (v.label == 'map')) then

			thisMap.width = v.xarg.width
			thisMap.height = v.xarg.height
			thisMap.tileWidth = v.xarg.tilewidth
			thisMap.tileHeight = v.xarg.tileheight
			thisMap.tiles = {}

			for l,u in pairs(v) do
				if ((type(u) == 'table') and (u.label == 'tileset')) then
					local imgPath = u[1].xarg.source
					local firstGid = u.xarg.firstgid
					local a,b,imgTag = string.find(imgPath, '.*/([^/]+)$')
					local img = ImageLibrary:loadImageFile('resource/maps/'..imgPath, true)
					local iw = img:getWidth()
					local ih = img:getHeight()
					local tw = thisMap.tileWidth
					local th = thisMap.tileHeight
					for c = 1,((iw * ih)/(tw * th)) do
						
						local qx = (c-1) % (iw / tw)
						local qy = math.floor((c-1) / (iw / tw))

						thisMap.tiles[c + firstGid - 1] = {}
						thisMap.tiles[c + firstGid - 1].imgPath = 'resource/maps/'..imgPath
						thisMap.tiles[c + firstGid - 1].quad = 
							love.graphics.newQuad(qx*tw, qy*th, tw, th, iw, ih)

					end

					-- Process per-tile properties
					for cn = 1,#u do
						if((type(u[cn]) == 'table') and (u[cn].label == 'tile')) then
							local tid = (tonumber(u[cn].xarg.id) or 1) + 1
							local tcoll = tonumber(u[cn][1][1].xarg.value) or 0
							thisMap.tiles[tid].coll = tcoll
						end
					end
				elseif ((type(u) == 'table') and (u.label == 'layer')) then
--[[
					local newLayer = {}
					newLayer.width = u.xarg.width
					newLayer.height = u.xarg.height
					newLayer.hidden = 0
					newLayer.data = {}

					for m,n in pairs(u[1]) do
						if (n.label == 'tile') then
							newLayer.data[#newLayer.data + 1] = tonumber(n.xarg.gid) or 1
						end
					end
]]--

					--print('Attempting to load layer '..u.xarg.name)

					local layerName = u.xarg.name
					local newLayer = LayerObject.new(u[1], thisMap.tiles)
					newLayer.name = u.xarg.name
					newLayer.width = tonumber(u.xarg.width)
					newLayer.height = tonumber(u.xarg.height)
					thisMap.layers[u.xarg.name] = newLayer

					--print('\tResult: '..type(newLayer)..'; data: '..#newLayer.data)

				end
			end

		end

	end

	self.maps[mapName] = thisMap

	if (self.maps[mapName] == nil) then return nil end

	self.activeMap = mapName

	self.dirty = true

end

--[[

	**TODO:	- Update to handle multiple layers.  Currently assumes just one, which is
		bad practice.

		- Include checks for the 'visible' attribute of the layer; don't render if
			visible == 'no'.

]]--

function MapObject:draw(layerName, camx, camy)

	local myLayer = this.layers[layerName]

	myLayer:draw(camx, camy)

end

