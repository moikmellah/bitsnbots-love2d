--[[
Copyright 2014 MoikMellah

This file is part of Bits & Bots.

Project SYPHA is free software: you can redistribute it and/or modify it under the terms
of the ZLIB License.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the ZLIB License for more details.

You should have received a copy of the ZLIB License along with
Bits & Bots. If not, see http://zlib.net/zlib_license.html
]]--

require('utils.utils')

defaultInputBindings = {
	up = {'key.up', 'pad.up'},
	down = {'key.down', 'pad.down'},
	left = {'key.left', 'pad.left'},
	right = {'key.right', 'pad.right'},
	jump = {'key. '},	-- Well, ain't that awkward.
	menu = {'key.escape'},
	swap = {'key.f11'},
	act1 = {'key.f'},
	act2 = {'key.d'},
	act3 = {'key.s'},
	throw = {'key.g'},
	block = {'key.r'},
	debug1 = {'key.f1'},
	debug2 = {'key.f2'},
	debug3 = {'key.f3'},
	mouseleft = {'mouse.1'}
}

-- Member fields template; used if no other template is given
InputLibraryDefaults = {

	key = {},

	input = {},

	luaClass = "InputLibrary",	-- These stick around.  They cool.
	class = "InputLibrary",
	name = "InputLibrary"

}

-- Table declaration for class
InputLibrary = {}

-- Metatable for class (to make InputLibrary table the default lookup for class methods)
InputLibrary_mt = {}
InputLibrary_mt.__index = InputLibrary

-- Constructor
function InputLibrary.new(template)

	-- Template ref
	local gTmp = nil

	-- Check if arg is a table; if not, use the default table declared above
	if type(template) == "table" then
		gTmp = template
	else
		gTmp = InputLibraryDefaults
	end

	-- Make copy from template if provided, or default template if not
	local gObj = tableCopy(gTmp)

	-- Set metatable to get our class methods
	setmetatable(gObj, InputLibrary_mt)

	-- Initialize with default map
	for ik,iv in pairs(defaultInputBindings) do
		for kk,kv in pairs(iv) do
			gObj:addBinding(ik,kv)
		end
	end

	gObj.mouseEvents = {}

	-- Return object ref
	return gObj

end

-- init: moving from instantiated to just a global instance.  Meh.
function InputLibrary:init()

	self.key = {}

	self.input = {}

	self.luaClass = "InputLibrary"	-- These stick around.  They cool.
	self.class = "InputLibrary"
	self.name = "InputLibrary"

	-- Initialize with default map
	for ik,iv in pairs(defaultInputBindings) do
		for kk,kv in pairs(iv) do
			self:addBinding(ik,kv)
		end
	end

	self.mouseEvents = {}

	self:grabInput()

	return true

end

function InputLibrary:resetKeyStates()

	for kk,vv in pairs(self.key) do
		self:setKeyState(kk,false)
	end

end

function InputLibrary:setKeyState(key, state)

	-- if state then print('key:'..key) end

	-- Exit if invalid type
	if type(key) ~= 'string' then return nil end

	-- Okay, we're valid - set the value and jet.
	self.key[key] = state

	return true

end

function InputLibrary:addInput(input)

	self.input[input] = {
		binding = {},
		len = 0,
		st = 'open',
		ack = false
	}

end

function InputLibrary.cbMousePress(x,y,key)
	InputLibrary:addMouseEvent(key, true)
end

function InputLibrary.cbMouseRelease(x,y,key)
	InputLibrary:addMouseEvent(key, false)
end

function InputLibrary:grabInput()

	love.keypressed = self.cbKeyPress 
	love.keyreleased = self.cbKeyRelease

	love.mousepressed = InputLibrary.cbMousePress
	love.mousereleased = InputLibrary.cbMouseRelease

end

--[[
	Callbacks, so can't use OO style of function call.
]]--
function InputLibrary.cbKeyPress(key)
	InputLibrary:setKeyState('key.'..key, true)
end

function InputLibrary.cbKeyRelease(key)
	InputLibrary:setKeyState('key.'..key, false)
end

function InputLibrary:addMouseEvent(key, state)
	table.insert(self.mouseEvents, {k = 'mouse.'..key, v = state})
end

function InputLibrary:addBinding(input, key)

	if type(self.input[input]) ~= 'table' then self:addInput(input) end

	table.insert(self.input[input].binding, key)

end

function InputLibrary:deleteBinding(input, key)

	if type(self.input[input]) ~= 'table' or type(self.input[input].binding ~= 'table') then return false end

	for k,v in pairs(self.input[input].binding) do
		if v == key then
			table.remove(self.input[input].binding, k)
		end
	end

end

function InputLibrary:updateInputStates(dt)

	-- Loop through all defined inputs
	for ik,iv in pairs(self.input) do
		local thisState = false

		-- Loop through all bindings for this input; if any bound key exists and is active, input state is true
		for bk,bv in pairs(iv.binding) do
			if (self.key[bv] ~= nil and self.key[bv] ~= 0 and self.key[bv] ~= false) then
				thisState = true
			end
		end

		-- If the input is active, increment our held length by the delta value
		if thisState == true then
			if self.input[ik].len == 0 then 
				self.input[ik].len = dt
				self.input[ik].ack = false
				self.input[ik].st = 'press'
			else
				self.input[ik].len = self.input[ik].len + dt
				self.input[ik].st = 'held'
			end
		else
			if self.input[ik].len > 0 then 
				self.input[ik].len = 0
				self.input[ik].ack = false
				self.input[ik].st = 'release'
			else
				self.input[ik].st = 'open'
			end
		end
	end

	-- Grab a mouse event off the cache, if there is one
	if(#self.mouseEvents > 0) then
		local ev = table.remove(self.mouseEvents, 1)
		self:setKeyState(ev.k, ev.v)
	end

end

--[[

	mousePos() - provides mouse coords, translated to screen position, where:
		0 < x < viewportWidth
		0 < y < viewportHeight

]]--
function InputLibrary:mousePos()

	local msx = love.mouse.getX() * vpWidth / love.graphics.getWidth()
	local msy = love.mouse.getY() * vpHeight / love.graphics.getHeight()

	return msx,msy

end

function InputLibrary:len(input)

	if type(self.input[input]) ~= 'table' then self:addInput(input) end

	return self.input[input].len

end

function InputLibrary:st(input)
	
	if type(self.input[input]) ~= 'table' then self:addInput(input) end

	return self.input[input].st

end

function InputLibrary:down(input)

	local st = self:st(input)

	if((st == 'press') or (st == 'held')) then return true end

	return false

end

function InputLibrary:ack(input)
	
	if type(self.input[input]) ~= 'table' then self:addInput(input) end

	return self.input[input].ack

end

function InputLibrary:setAck(input, ack)

	if type(self.input[input]) ~= 'table' then self:addInput(input) end

	if (ack) then
		self.input[input].ack = true
	else
		self.input[input].ack = false
	end

end

-- Simple test of class members
function InputLibrary:whoAmI()

	if self.name ~= nil then print("My name is "..self.name) else print("I am nameless.") end

end

-- ToString method (needs work)
function InputLibrary:toString()

	local str = ""

	if self.name ~= nil then str = self.name
	elseif self.class ~= nil then str = self.class
	elseif self.luaClass ~= nil then str = self.luaClass
	else str = "InputLibrary" end

	return str

end
