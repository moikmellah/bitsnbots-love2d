--[[
Copyright 2014 MoikMellah

This file is part of Bits & Bots.

Project SYPHA is free software: you can redistribute it and/or modify it under the terms
of the ZLIB License.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the ZLIB License for more details.

You should have received a copy of the ZLIB License along with
Bits & Bots. If not, see http://zlib.net/zlib_license.html
]]--

Map = {}

Map.init = function(mapIndex)

	AudioLibrary:stopAll()
	-- love.audio.stop()

	fadeColor = 0

	local mapNum = mapIndex
	if mapNum == nil then mapNum = 1 end

	local thisMap = maps[mapNum]

	if thisMap == nil then return end

	objs = {}

	for k,t in pairs(thisMap.templates) do

		if t.init == 1 then objs[#objs+1] = GameObject.new(t) end

	end

	cams = {}

	if(mapLib[thisMap.filePath]) == nil then
		mapLib[thisMap.filePath] = MapObject.new(thisMap.filePath)
	end

	-- **Todo: init camera at a predefined index
	for k,t in pairs(thisMap.cameras) do

--		local vk = #cams+1

		if t.init > 0 and cams[t.init] == nil then 
			cams[t.init] = BkgObject.new(t) 
			cams[t.init].myLayer = mapLib[thisMap.filePath].layers[cams[t.init].layerName]
			objs[#objs+1] = cams[t.init]
			-- print('Camera '..t.init..' installed at list pos '..#objs)
		end

	end

--	if thisMap.bkgMusic then
--		AudioLibrary:loadAudioFile(thisMap.bkgMusic, 1)
--		AudioLibrary:play(thisMap.bkgMusic, true)
--	end

	if type(thisMap.init) == "function" then thisMap.init() end

end

