--[[
Copyright 2014 MoikMellah

This file is part of Bits & Bots.

Project SYPHA is free software: you can redistribute it and/or modify it under the terms
of the ZLIB License.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the ZLIB License for more details.

You should have received a copy of the ZLIB License along with
Bits & Bots. If not, see http://zlib.net/zlib_license.html
]]--

Robot = {}

Robot.setup = function(self,dtA)

	local dummy = nil
	local dt = dtA

	-- Lock our buttons down until we're all ready (probably need to rework this, but whatever)
	inputOk = false

	-- Set initial image
	self.quad = 5
	--self.iOfsX, self.iOfsY = -12, -12
	self.facing = 1

	-- Sit still for 1 second
	local cc = 0
	local limit = 1
	while cc < limit do
		cc = cc + dt
		dummy,dt = coroutine.yield()
	end

	-- Calculate our starting position
	local tx = ((self.gx - 1) * 24) + 21
	local ty = ((self.gy - 1) * 24) + 14

	-- Move to our starting position
	Robot.move(self,tx,ty,dt)

	-- Unlock our buttons
	inputOk = true

	-- Switch to game logic
	self.nextLogic = Robot.waitCmd

	-- FIN
	return nil

end

--[[

	waitForCmd() - Robot waits patiently for a command.

]]--

Robot.waitCmd = function(self,dtA)

	local dummy = nil
	local dt = dtA

	local breakLoop = false

	while breakLoop == false do

		self.quad = 5
		--self.iOfsX, self.iOfsY = -12, -12
		self.facing = 1

		-- Continue to yield if no command has been given, or we've already checked in on the current one
		if cmdGo ~= 1 or botCheck[self.botId] == true then
			--if Input.cKeyPressed(INPUT_BSEL) > 0 then --DIAG - remove
			--	self.gx = self.wx
			--	self.gy = self.wy
			--end
			dummy,dt = coroutine.yield()
		-- Move if we're receiving a command
		elseif btnVal['BOT'][self.botId] == 1 then
			grid[self.gx][self.gy] = self.botId
			dummy,dt = coroutine.yield()
			Robot.verifyCmd(self,dt)
		-- None of the above?  Check in and keep on keepin' on..
		else
			botCheck[self.botId] = true
			grid[self.gx][self.gy] = self.botId + 10
		end

		if self.gx == self.wx and self.gy == self.wy then
			-- Robot.warp(self,260,110 + (18 * self.botId))
			breakLoop = true
			self.nextLogic = Robot.retire
			botWin[self.botId] = true
			grid[self.gx][self.gy] = 0
		end

	end

	return nil

end

--[[

	verifyCmd() - Robot vets command to see if it applies

]]--

Robot.verifyCmd = function(self,dtA)

	local dummy = nil
	local dt = dtA

	local dirInt = 0

	-- Calculate our 'move value'
	if btnVal.DIR.U == 1 then dirInt = dirInt + 1 end
	if btnVal.DIR.D == 1 then dirInt = dirInt + 2 end
	if btnVal.DIR.L == 1 then dirInt = dirInt + 4 end
	if btnVal.DIR.R == 1 then dirInt = dirInt + 8 end

	-- First condition: two moves requested, opposites
	if dirInt == 3 or dirInt == 12 then

		-- Do 'confusion', then return to waitCmd state
		-- print("No.")
		-- self.nextLogic = Robot.waitCmd
		botCheck[self.botId] = true
		return false

	-- Up and left
	elseif dirInt == 5 then

		self.moves[1] = { x = 0, y = -1 }
		self.moves[2] = { x = -1, y = 0 }
		Robot.execCmd(self,dt)

	-- Up and right
	elseif dirInt == 9 then

		self.moves[1] = { x = 0, y = -1 }
		self.moves[2] = { x = 1, y = 0 }
		Robot.execCmd(self,dt)

	-- Down/Left
	elseif dirInt == 6 then

		self.moves[1] = { x = 0, y = 1 }
		self.moves[2] = { x = -1, y = 0 }
		Robot.execCmd(self,dt)

	-- Down/Right
	elseif dirInt == 10 then

		self.moves[1] = { x = 0, y = 1 }
		self.moves[2] = { x = 1, y = 0 }
		Robot.execCmd(self,dt)

	-- Up
	elseif dirInt == 1 then

		self.moves[1] = { x = 0, y = -1 }
		Robot.execCmd(self,dt)

	-- Down
	elseif dirInt == 2 then

		self.moves[1] = { x = 0, y = 1 }
		Robot.execCmd(self,dt)

	-- Left
	elseif dirInt == 4 then

		self.moves[1] = { x = -1, y = 0 }
		Robot.execCmd(self,dt)

	-- Right
	elseif dirInt == 8 then

		self.moves[1] = { x = 1, y = 0 }
		Robot.execCmd(self,dt)

	-- All else (0, 3, or 4 directions)
	else
		-- Check in and bail
		botCheck[self.botId] = true
		return false
	end

	return false

end

--[[

	acknowledgeCmd() - Robot signals that it has work to do (..nah)

]]--



--[[

	analyzeCmd() - Robot checks board to see if it can move (..also nah)

]]--



--[[

	execCmd() - Robot moves to new position

]]--

Robot.execCmd = function(self,dtA)

	local dummy = nil
	local dt = dtA

	-- Target move params, for moving and stuff.
	local tgx = 0
	local tgy = 0

	-- Timer for waiting for a spot to clear
	local clearTimer = 0
	local clearThresh = 0.125

	-- Loop through and execute each move in sequence.
	for k,v in pairs(self.moves) do

		-- Initialize our vars
		tgx = self.gx + v.x
		tgy = self.gy + v.y
		clearTimer = 0

		-- If our target position is occupied, wait a few cycles to see if it moves
		while clearTimer < clearThresh and grid[tgx][tgy] > 0 and grid[tgx][tgy] < 9 do
			-- Increment timer, and back off
			clearTimer = clearTimer + dt
			dummy,dt = coroutine.yield()
		end

		-- If it's clear, update our pos and move there
		if grid[tgx][tgy] == 0 then

			-- Update grid, as well as our pos vars
			grid[self.gx][self.gy] = 0
			grid[tgx][tgy] = self.botId
			self.gx,self.gy = tgx,tgy

			local tx = ((tgx - 1) * 24) + 21
			local ty = ((tgy - 1) * 24) + 14

			-- Now, do the actual move
			Robot.move(self,tx,ty,dt)

		end


		-- Finally, clear out the current move
		self.moves[k] = nil

	end

	botCheck[self.botId] = true

	return true

end

--[[

	move() - Execute the given move

--]]

Robot.move = function(self, xt, yt, dtA)

	local dummy = nil
	local dt = dtA

	-- table of animation frames to rotate through, moving left or right
	local imgs_lr = {}
	imgs_lr[1] = 1
	imgs_lr[2] = 2
	imgs_lr[3] = 3
	imgs_lr[4] = 4

	-- table of frames to rotate through, moving upward
	local imgs_up = {}
	imgs_up[1] = 12
	imgs_up[2] = 11
	imgs_up[3] = 10
	imgs_up[4] = 9

	-- table of frames to rotate through, moving downward
	local imgs_down = {}
	imgs_down[1] = 5
	imgs_down[2] = 6
	imgs_down[3] = 7
	imgs_down[4] = 8

	-- reference to whichever table above is appropriate
	local imgs = nil

	-- current animation table index and timers
	local imgnum = 0
	local cFrame = 0
	local tFrame = 2/32

	-- Change this to alter the robot's moving speed
	local xvel = 35
	local yvel = 35

	local xv = 0
	local yv = 0

	-- Right
	if xt > self.x then

		-- setup for moving right
		imgs = imgs_lr
		self.quad = imgs[imgnum + 1]
		self.facing = -1
		--self.iOfsX, self.iOfsY = -6, -12
		xv = xvel

	-- Left
	elseif xt < self.x then

		-- setup for moving left
		imgs = imgs_lr
		self.quad = imgs[imgnum + 1]
		self.facing = 1
		--self.iOfsX, self.iOfsY = -8, -12
		xv = -xvel

	end

	-- Down
	if yt > self.y then

		-- setup for moving down
		imgs = imgs_down
		self.quad = imgs[imgnum + 1]
		self.facing = 1
		--self.iOfsX, self.iOfsY = -12, -12
		yv = yvel

	-- Up
	elseif yt < self.y then

		-- setup for moving upward
		imgs = imgs_up
		self.quad = imgs[imgnum + 1]
		self.facing = 1
		--self.iOfsX, self.iOfsY = -12, -12
		yv = -yvel

	end

	-- Okay, now MOVE IT!
	while self.x ~= xt or self.y ~= yt do

		self.x = self.x + (xv * dt)
		self.y = self.y + (yv * dt)

		if math.abs(self.x - xt) < (xvel * dt) then
			self.x = xt
			xv = 0
		end

		if math.abs(self.y - yt) < (yvel * dt) then
			self.y = yt
			yv = 0
		end

		cFrame = cFrame + dt
		if cFrame > tFrame then
			cFrame = 0
			imgnum = (imgnum + 1) % 4
			self.quad = imgs[imgnum + 1]
		end
		dummy,dt = coroutine.yield()

	end

end

--[[

	refuseCmd() - Robot gives some indication that it is confused (can't exec command)

]]--

--[[

	warp() - four-cell animation, warps bot to a new location (nx,ny)

]]--

Robot.warp = function(self, nx, ny)

	-- Animation for 'dissolving' effect
	local imgs = {}
	imgs[1] = 5
	imgs[2] = 13
	imgs[3] = 14
	imgs[4] = 15
	imgs[5] = 16

	-- Reverse of above
	local imgsx = {}
	imgsx[5] = 5
	imgsx[4] = 13
	imgsx[3] = 14
	imgsx[2] = 15
	imgsx[1] = 16

	local cFrame = 0
	local cTimer = 2

	local k = nil
	local v = nil

	self.facing = 1
	--self.iOfsX, self.iOfsY = -12, -12

	AudioLibrary:play('sfx/warp.ogg')

	-- First, loop through frames forward
	for k,v in pairs(imgs) do
		self.quad = v
		for cFrame = 1,cTimer do coroutine.yield() end
	end

	-- Set new coords
	self.x,self.y = nx,ny

	-- Loop through other frameset
	for k,v in pairs(imgsx) do
		self.quad = v
		for cFrame = 1,cTimer do coroutine.yield() end
	end

	return nil

end

--[[

	retire() - We've won.  Warp to the sidebar, then dance forever.  FOREVER.

]]--

Robot.retire = function(self,dtA)

	local dummy = nil
	local dt = dtA

	local cx = 260
	local cy = 105 + (18 * self.botId)

	Robot.warp(self,cx,cy)

	while 1 == 1 do

		Robot.move(self, cx + 30, cy, dt)
		Robot.botSpin(self, true, dt)
		Robot.move(self, cx - 30, cy, dt)
		Robot.botSpin(self, false, dt)

	end	

end

--[[

	botSpin - bot spins around.  Dance, you fool!!

]]--

Robot.botSpin = function(self, spinRight, dtA)

	local dummy = nil
	local dt = dtA

	-- Order: 11, 0, 4, 0(reversed)

	local fdelay = 6/32
	local ftimer = 0

	if spinRight then
		self.quad = 5
		--self.iOfsX, self.iOfsY = -12, -12
		-- for for ftimer = 1,fdelay do coroutine.yield() end
		ftimer = 0
		while ftimer < fdelay do
			ftimer = ftimer + dt
			dummy,dt = coroutine.yield()
		end

		self.quad = 1
		self.facing = 1
		--self.iOfsX, self.iOfsY = -5, -12
		-- for for ftimer = 1,fdelay do coroutine.yield() end
		ftimer = 0
		while ftimer < fdelay do
			ftimer = ftimer + dt
			dummy,dt = coroutine.yield()
		end
	
		self.facing = -1
		self.quad = 12
		--self.iOfsX, self.iOfsY = -12, -12
		-- for for ftimer = 1,fdelay do coroutine.yield() end
		ftimer = 0
		while ftimer < fdelay do
			ftimer = ftimer + dt
			dummy,dt = coroutine.yield()
		end

		self.quad = 1
		--self.iOfsX, self.iOfsY = -7, -12
		-- for for ftimer = 1,fdelay do coroutine.yield() end
		ftimer = 0
		while ftimer < fdelay do
			ftimer = ftimer + dt
			dummy,dt = coroutine.yield()
		end

		self.quad = 5
		--self.iOfsX, self.iOfsY = -12, -12
		-- for for ftimer = 1,fdelay do coroutine.yield() end
		ftimer = 0
		while ftimer < fdelay do
			ftimer = ftimer + dt
			dummy,dt = coroutine.yield()
		end

	else
		self.quad = 5
		--self.iOfsX, self.iOfsY = -12, -12
		-- for for ftimer = 1,fdelay do coroutine.yield() end
		ftimer = 0
		while ftimer < fdelay do
			ftimer = ftimer + dt
			dummy,dt = coroutine.yield()
		end

		self.facing = -1
		self.quad = 1
		--self.iOfsX, self.iOfsY = -5, -12
		-- for for ftimer = 1,fdelay do coroutine.yield() end
		ftimer = 0
		while ftimer < fdelay do
			ftimer = ftimer + dt
			dummy,dt = coroutine.yield()
		end

		self.facing = 1
		self.quad = 12
		--self.iOfsX, self.iOfsY = -12, -12
		-- for for ftimer = 1,fdelay do coroutine.yield() end
		ftimer = 0
		while ftimer < fdelay do
			ftimer = ftimer + dt
			dummy,dt = coroutine.yield()
		end

		self.quad = 1
		--self.iOfsX, self.iOfsY = -7, -12
		-- for for ftimer = 1,fdelay do coroutine.yield() end
		ftimer = 0
		while ftimer < fdelay do
			ftimer = ftimer + dt
			dummy,dt = coroutine.yield()
		end

		self.quad = 5
		--self.iOfsX, self.iOfsY = -12, -12
		-- for for ftimer = 1,fdelay do coroutine.yield() end
		ftimer = 0
		while ftimer < fdelay do
			ftimer = ftimer + dt
			dummy,dt = coroutine.yield()
		end

	end
	
end

