--[[
Copyright 2014 MoikMellah

This file is part of Bits & Bots.

Project SYPHA is free software: you can redistribute it and/or modify it under the terms
of the ZLIB License.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the ZLIB License for more details.

You should have received a copy of the ZLIB License along with
Bits & Bots. If not, see http://zlib.net/zlib_license.html
]]--

Textbox = {}

--[[

scoreDisplay() - textbox for displaying Score data

]]--

Textbox.scoreDisplay = function(self, dtA)

	local dummy = nil
	local dt = dtA

	local lastTime = 0
	local curTime = 0
	local niceMin = 0
	local niceSec = 0
	local niceTicks = 0
	local niceMoves = 0

	while 1 == 1 do

		-- Fetch current tick count
		curTime = curTime + dt

		-- Don't update score.time if we've already won
		if (botCheck[1] == false or botCheck[2] == false 
			or botCheck[3] == false or botCheck[4] == false) then
			score.time = score.time + dt
		end

		-- 'Ticks' portion - extract and pad, if necessary
		niceTicks = math.floor(score.time * 100) % 100
		niceTicks = tostring(niceTicks)
		if #niceTicks == 1 then niceTicks = "0"..niceTicks end

		-- 'Secs' portion - extract and pad, if necessary
		niceSec = math.floor(score.time) % 60
		niceSec = tostring(niceSec)
		if #niceSec == 1 then niceSec = "0"..niceSec end

		-- 'Mins' portion - no padding necessary.
		niceMin = math.floor(score.time / 60)

		-- Convert and pad score.moves
		niceMoves = tostring(score.moves)
		if #niceMoves == 1 then niceMoves = "0"..niceMoves end
		if #niceMoves == 2 then niceMoves = "0"..niceMoves end

		-- Output time and moves to a temp string
		--local tmpStr = "Time: "..niceMin..":"..niceSec.."."..niceTicks.."\n"
		--tmpStr = tmpStr.."Moves:   "..niceMoves.."\n"
		local tmpStr = niceMin..":"..niceSec.."."..niceTicks.."\n"
		tmpStr = tmpStr..niceMoves.."\n"

		-- Update label
		self.label.text = tmpStr

		dummy,dt = coroutine.yield()

	end

end

-- logoDisplay() - display the logo

Textbox.logoDisplay = function(self)

	while 1 == 1 do

		Textbox.checkActive(self)

		coroutine.yield()

	end

end

-- displayWin() - Hurray, you won!

Textbox.displayWin = function(self)

	-- Do nothing until bots have all reached their goals
	while botWin[1] == false or botWin[2] == false or
		botWin[3] == false or botWin[4] == false do
		coroutine.yield()
	end

	-- Activate both the label and the background rect
	self.label.visible = 1
	--self.prim.visible = 1

	-- Other variables for calculating and displaying score
	local niceTicks = 0
	local niceSec = 0
	local niceMin = 0
	local niceMoves = 0
	local niceScore = 0
	local niceRank = "Yikes. Try again."
	local msx = 0
	local msy = 0

	-- Calculate 'ticks' portion of timer
	niceTicks = math.floor(score.time * 100) % 100
	niceTicks = tostring(niceTicks)
	if #niceTicks == 1 then niceTicks = "0"..niceTicks end

	-- 'Secs' portion
	niceSec = math.floor(score.time) % 60
	niceSec = tostring(niceSec)
	if #niceSec == 1 then niceSec = "0"..niceSec end

	-- 'Mins' portion
	niceMin = math.floor(score.time / 60)

	-- Convert and pad score.moves
	niceMoves = tostring(score.moves)

	-- Determine final score (lower is better!)
	niceScore = math.floor(score.time * 10) + (niceMoves * 50)

	-- Determine ranking, according to scoring table above
	for k,v in pairs(rankTable) do
		if (niceScore >= v.min) and (niceScore <= v.max) then niceRank = v.rank end
	end

	-- Output time and moves to a temp string
	local tmpStr = "You win!\n"
	tmpStr = tmpStr.."You made "..niceMoves.." moves in "..niceMin..":"..niceSec.."."..niceTicks..".\n"
	tmpStr = tmpStr.."Your rating: "..niceRank.."\n\n"
	tmpStr = tmpStr.."Click here to play again!"

	-- Update label
	self.label.text = tmpStr

	--print('Displaying scores.')

	-- Loop, wait for user input
	while 1 == 1 do

		msx,msy = self:mousePos() --Input.camMouse(self.cam)

		if (self:labelCollision(msx,msy)) and ((InputLibrary:st('mouseleft') == 'press') and not(InputLibrary:ack('mouseleft'))) then
			InputLibrary:setAck('mouseleft', true)
			local bc = 0
			for bc = 1,32 do
				--fadeColor = (bc * 8) - 1
				screenfx.r = 1 - bc/32
				screenfx.g = 1 - bc/32
				screenfx.b = 1 - bc/32
				AudioLibrary:setMaster(1 - bc/32)
				coroutine.yield()
			end
			loadRequest = 1
		end

		coroutine.yield()

	end

end

Textbox.loopRight = function(self, dtA)

	local dt = dtA

	-- Scrolling velocity
	local xvel = 0

	local dummy = nil

	-- Fetch from obj
	if type(self.xvel) == "number" then xvel = self.xvel end

	-- Loop threshold
	local xloop = 640+160

	-- Fetch from obj
	if type(self.xloop) == "number" then xloop = self.xloop end

	-- Loop!
	while 1 == 1 do

		self.x = self.x + (self.xvel * dt)

		--if Input.cKeyLen(self.visBtn) > 0 then self.visible = 0 else self.visible = 1 end

		if self.x > xloop then self.x = self.x - 640 end

		dummy, dt = coroutine.yield()

	end

end

Textbox.hoverYellow = function(self)

	local regColor = {r=.14,g=.23,b=.71,a=.38} --0xfffc9d60 --0xff222260
	local hovColor = {r=.14,g=.56,b=.08,a=.38}
	local camX = 0
	local camY = 0
	local msx = 0
	local msy = 0

	camX,camY = self:getCamCoords()

	while 1 == 1 do

		msx,msy = self:mousePos() --Input.camMouse(self.cam) --cMousePos()

		if self:labelCollision(msx,msy) then
			self.label.bkgColor = hovColor
		else
			self.label.bkgColor = regColor
		end

		coroutine.yield()

	end

end

Textbox.menuPlay = function(self,dtA)

	local dummy = nil
	local dt = dtA

	local regColor = {r=.14,g=.23,b=.71,a=.38} --0xfffc9d60 --0xff222260
	local hovColor = {r=.14,g=.56,b=.08,a=.38}
	local brd = {r=1,g=1,b=1,a=1,w=2}
	local camX = 0
	local camY = 0
	local msx = 0
	local msy = 0

	local track = 1
	local iter = -1

	camX,camY = self:getCamCoords()

	self.label.brd = self.label.brd or brd

	while 1 == 1 do

		Textbox.checkActive(self)

		msx,msy = self:mousePos() --Input.camMouse(self.cam) --cMousePos()

		if self:labelCollision(msx,msy) then
			--self.label.bkgColor = hovColor

			--EDIT ME TO ENABLE GAMEPLAY (remove 'false and ') --
			if (InputLibrary:st('mouseleft') == 'press') and not(InputLibrary:ack('mouseleft')) and inputOk == 1 then
				AudioLibrary:play('sfx/blip.ogg')
				InputLibrary:setAck('mouseleft', true)
				local bc = 0
				for bc = 1,32 do
					--fadeColor = (bc * 8) - 1
					screenfx.r = 1 - bc/32
					screenfx.g = 1 - bc/32
					screenfx.b = 1 - bc/32
					AudioLibrary:setMaster(1 - bc/32)
					dummy,dt = coroutine.yield()
				end
				loadRequest = 1
			end
		--else
			--self.label.bkgColor = regColor
		end

		dummy,dt = coroutine.yield()

		track = track + (iter * dt)
		if((track > 1) or (track < 0)) then
			iter = iter * -1 
			track = math.max(0,math.min(track,1))
			--scrMirror.x = iter --hehe flippety-doo
		end

		self.label.brd.a = .3 + (.7*track)
		--self.label.brd.w = .75 + (track)

	end

end

Textbox.menuQuit = function(self, dtA)

	local dummy = nil
	local dt = dtA
	local track = 1
	local iter = -1

	local regColor = {r=.14,g=.23,b=.71,a=.38} --0xfffc9d60 --0xff222260
	local hovColor = {r=.14,g=.56,b=.08,a=.38}
	local brd = {r=1,g=1,b=1,a=1,w=2}
	local camX = 0
	local camY = 0
	local msx = 0
	local msy = 0

	self.label.brd = self.label.brd or brd

	camX,camY = self:getCamCoords()

	while 1 == 1 do

		Textbox.checkActive(self)

		msx,msy = self:mousePos() --Input.camMouse(self.cam) --cMousePos()

		if self:labelCollision(msx,msy) then
			-- self.label.bkgColor = hovColor
			if (InputLibrary:st('mouseleft') == 'press') and not(InputLibrary:ack('mouseleft')) and inputOk == 1 then
				AudioLibrary:play('sfx/blip.ogg')
				InputLibrary:setAck('mouseleft', true)
				local bc = 0
				for bc = 1,32 do
					--fadeColor = (bc * 8) - 1
					screenfx.r = 1 - bc/32
					screenfx.g = 1 - bc/32
					screenfx.b = 1 - bc/32
					AudioLibrary:setMaster(1 - bc/32)
					dummy,dt = coroutine.yield()
				end

				--Input.cQuit()
				love.event.quit()

			end
		--else
			--self.label.bkgColor = regColor
		end

		if (InputLibrary:st('menu') == 'press') and not(InputLibrary:ack('menu')) then
			AudioLibrary:play('sfx/blip.ogg')

			local bc = 0
			for bc = 1,32 do
				--fadeColor = (bc * 8) - 1
				screenfx.r = 1 - bc/32
				screenfx.g = 1 - bc/32
				screenfx.b = 1 - bc/32
				AudioLibrary:setMaster(1 - bc/32)
				dummy,dt = coroutine.yield()
			end

			--Input.cQuit()
			love.event.quit()

		end

		dummy,dt = coroutine.yield()

		track = track + (iter * dt)
		if((track > 1) or (track < 0)) then 
			iter = iter * -1 
			track = math.max(0,math.min(track,1))
			--scrMirror.x = iter --hehe flippety-doo
		end

		self.label.brd.a = .3 + (.7*track)

	end

end

Textbox.menuInstr = function(self,dtA)

	local dummy = nil
	local dt = dtA
	local track = 1
	local iter = -1

	local regColor = {r=.14,g=.23,b=.71,a=.38} --0xfffc9d60 --0xff222260
	local hovColor = {r=.14,g=.56,b=.08,a=.38}
	local brd = {r=1,g=1,b=1,a=1,w=2}
	local camX = 0
	local camY = 0
	local msx = 0
	local msy = 0

	self.label.brd = self.label.brd or brd

	camX,camY = self:getCamCoords()

	while 1 == 1 do

		Textbox.checkActive(self)

		msx,msy = self:mousePos() --Input.camMouse(self.cam) --cMousePos()

		if self:labelCollision(msx,msy) then
			--self.label.bkgColor = hovColor
			if (InputLibrary:st('mouseleft') == 'press') and not(InputLibrary:ack('mouseleft')) and inputOk == 1 then
				AudioLibrary:play('sfx/blip.ogg')
				InputLibrary:setAck('mouseleft', true)
				local instr = self:spawn(maps[2].templates[8],160,20)
				instr.label.text = instrStr
				instr.label.yofs = 40
				inputOk = 0
				hideMenu = 1
				if(self.debug) then
					print('Objs:'..#objs)
					for k,v in pairs(objs) do
						print('\n'..k..':'..v:toString())
					end
				end
				dummy,dt = coroutine.yield()
			end
		--else
			--self.label.bkgColor = regColor
		end

		dummy,dt = coroutine.yield()

		track = track + (iter * dt)
		if((track > 1) or (track < 0)) then 
			iter = iter * -1 
			track = math.max(0,math.min(track,1))
			--scrMirror.x = iter --hehe flippety-doo
		end

		self.label.brd.a = .3 + (.7*track)

	end

end

Textbox.menuCred = function(self,dtA)

	local dummy = nil
	local dt = dtA
	local track = 1
	local iter = -1

	local regColor = {r=.14,g=.23,b=.71,a=.38} --0xfffc9d60 --0xff222260
	local hovColor = {r=.14,g=.56,b=.08,a=.38}
	local brd = {r=1,g=1,b=1,a=1,w=2}
	local camX = 0
	local camY = 0
	local msx = 0
	local msy = 0

	self.label.brd = self.label.brd or brd

	camX,camY = self:getCamCoords()

	while 1 == 1 do

		Textbox.checkActive(self)

		msx,msy = self:mousePos() --Input.camMouse(self.cam) --cMousePos()

		if self:labelCollision(msx,msy) then
			--self.label.bkgColor = hovColor
			if (InputLibrary:st('mouseleft') == 'press') and not(InputLibrary:ack('mouseleft')) and inputOk == 1 then
				AudioLibrary:play('sfx/blip.ogg')
				InputLibrary:setAck('mouseleft', true)
				local instr = self:spawn(maps[2].templates[8],160,20)
				instr.label.text = aboutStr
				instr.label.align = 'left'
				instr.label.font = 'small'
				instr.label.scrollme = true
				instr.label.w = 280
				instr.label.x = -140
				instr.label.yofs = instr.label.h
				inputOk = 0
				hideMenu = 1
				dummy,dt = coroutine.yield()
			end
		--else
			--self.label.bkgColor = regColor
		end

		dummy,dt = coroutine.yield()

		track = track + (iter * dt)
		if((track > 1) or (track < 0)) then 
			iter = iter * -1 
			track = math.max(0,math.min(track,1))
			--scrMirror.x = iter --hehe flippety-doo
		end

		self.label.brd.a = .3 + (.7*track)

	end

end



Textbox.checkActive = function(self)

	if hideMenu == 1 then

		local visBk = self.visible
		local labBk = false

		self.visible = 0
		if type(self.label) == "table" then
			labBk = self.label.visible
			self.label.visible = 0
		end

		while hideMenu == 1 do
			coroutine.yield()
		end

		self.visible = visBk
		if type(self.label) == "table" then
			self.label.visible = labBk
		end

		-- Yield extra frame, so as not to register spurious clicking
		coroutine.yield()

	end

end

Textbox.waitMyTurn = function(self, dtA)

	local dt = dtA
	local dummy = nil

	while turn < self.turn do

		dummy,dt = coroutine.yield()

	end

	Textbox.flyOnScreen(self, dt)

	turn = turn + 1

--[[	if(self.playMusic) then
		AudioLibrary:play(menuMusic, true)
	end
]]--
	self.nextLogic = self.menuLogic

end

Textbox.flyOnScreen = function(self, dtA)

	local dt = dtA
	local dummy = nil

	-- Set velocity
	local xvel = self.xvel
	local yvel = self.yvel

	-- The distance at which we reduce speed by half is vel * dfct - hopefully this makes us glide smoothly to the target
	local dfct = .05

	-- Set targets
	local xtgt = self.xtgt
	local ytgt = self.ytgt

	local doneMoving = false

	-- Screen for nils
	if xvel == nil then xvel = 0 end
	if yvel == nil then yvel = 0 end
	if xtgt == nil then xtgt = self.x end
	if ytgt == nil then ytgt = self.y end

	-- Make sure we don't overshoot
--	if math.abs(xvel) > math.abs(xtgt - self.x) then xvel = xtgt - self.x end
--	if math.abs(yvel) > math.abs(ytgt - self.y) then yvel = ytgt - self.y end

	-- Kill vel if we're already there on that axis
	if self.x == xtgt then xvel = 0 end
	if self.y == ytgt then yvel = 0 end

	-- Make sure we're headed in the right direction
	if xvel < 0 and not ((xtgt - self.x) < 0) then xvel = xvel * -1 end
	if yvel < 0 and not ((ytgt - self.y) < 0) then yvel = yvel * -1 end

	-- Skip this whole thing if we're not moving or already there
	if ((xvel == 0) or (xtgt == self.x)) and ((yvel == 0) or (ytgt == self.y)) then doneMoving = true end

	local oneAudio = true

	local initxv = xvel
	local inityv = yvel

	-- Loop!
	while doneMoving == false do

		-- Move
		self.x = self.x + xvel * dt
		self.y = self.y + yvel * dt

		-- Slow down as we approach our target
		if math.abs(self.x - xtgt) < math.abs(xvel * dfct) then xvel = xvel / 2 end
		if math.abs(self.y - ytgt) < math.abs(yvel * dfct) then yvel = yvel / 2 end

		-- Play our audio once when first slowing down
		if(oneAudio and ((math.abs(self.x - xtgt) < math.abs(self.sfxthresh)) and (math.abs(self.y - ytgt) < math.abs(self.sfxthresh)))) then
			oneAudio = false
			if(self.sfx ~= nil) then
				AudioLibrary:play(self.sfx, false)
			end
		end

		-- Make sure we don't overshoot
		--if math.abs(xvel * dt) > math.abs(xtgt - self.x) then xvel = xtgt - self.x end
		--if math.abs(yvel * dt) > math.abs(ytgt - self.y) then yvel = ytgt - self.y end
		if math.abs(xvel * dt) > math.abs(xtgt - self.x) then self.x = xtgt end
		if math.abs(yvel * dt) > math.abs(ytgt - self.y) then self.y = ytgt end

		-- If we're within 1 pixel, just jump to dest.
		if math.abs(xtgt - self.x) < 1 then xvel = 0 self.x = xtgt end
		if math.abs(ytgt - self.y) < 1 then yvel = 0 self.y = ytgt end

		-- Are we done yet?
		if self.x == xtgt and self.y == ytgt then doneMoving = true end

		dummy,dt = coroutine.yield()

	end

--	if(self.sfx ~= nil) then
--		AudioLibrary:play(self.sfx, false)
--	end

	return true

end

Textbox.displayInstr = function(self,dtA)

	local dummy = nil
	local dt = dtA

	local regColor = {r=.14,g=.23,b=.71,a=.38} --0xfffc9d60 --0xff222260
	local hovColor = {r=.14,g=.56,b=.08,a=.38}
	local camX = 0
	local camY = 0
	local msx = 0
	local msy = 0

	local scspd = -12

	local keepGoing = 1

	local hwid = 0
	local htot = 0
	local htab = nil

	self.label.font = self.label.font or 'default'

	local myFont = fontLib[self.label.font]

	hwid,htab = myFont:getWrap(self.label.text, self.label.w)

	htot = #htab

	htot = htot * myFont:getHeight()

--	if (type(self.label) == "table") then self.label.text = instrStr end

	camX,camY = self:getCamCoords()

	dummy,dt = coroutine.yield()

	while keepGoing == 1 do

		if self.label.scrollme then
			if self.label.yofs < htot * -1 then self.label.yofs = self.label.h end
			self.label.yofs = self.label.yofs + (scspd * dt)
		end

		msx,msy = self:mousePos() --Input.camMouse(self.cam) --cMousePos()

		if self:labelCollision(msx,msy) then
			if (InputLibrary:st('mouseleft') == 'press') and not(InputLibrary:ack('mouseleft')) then
				AudioLibrary:play('sfx/blip.ogg')
				InputLibrary:setAck('mouseleft', true)
				inputOk = 1
				hideMenu = 0
				keepGoing = 0
			end
		end

		if (InputLibrary:st('menu') == 'press') and not(InputLibrary:ack('menu')) then
			AudioLibrary:play('sfx/blip.ogg')
			InputLibrary:setAck('menu', true)
			inputOk = 1
			hideMenu = 0
			keepGoing = 0
		end

		dummy,dt = coroutine.yield()

	end

	self.killme = 1

end

Textbox.displayPresents = function(self, dtA)

	local dummy = nil
	local dt = dtA

	local dc = 1

	while dc > 0 do

		self.label.color.a = (1 - dc)

		dc = dc - dt

		dummy,dt = coroutine.yield()

	end

	--visible = 0
	dc = 1

	self.label.color.a = 1

	while dc > 0 do 
		dummy,dt = coroutine.yield() 
		dc = dc - dt 
	end

	dc = 1

	while dc > 0 do 

		self.label.color.a = dc

		dc = dc - dt

		dummy,dt = coroutine.yield()

	end

	turn = turn + 1

	self.killme = true

end
