--[[
Copyright 2014 MoikMellah

This file is part of Bits & Bots.

Project SYPHA is free software: you can redistribute it and/or modify it under the terms
of the ZLIB License.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the ZLIB License for more details.

You should have received a copy of the ZLIB License along with
Bits & Bots. If not, see http://zlib.net/zlib_license.html
]]--

require('utils.xml')

-- Table declaration for class
ImageLibrary = {}

-- Metatable for class (to make ImageLibrary table the default lookup for class methods)
ImageLibrary_mt = {}
ImageLibrary_mt.__index = ImageLibrary

-- Constructor
function ImageLibrary.new()

	-- Make copy from template if provided, or default template if not
	local nImgLib = {}

	-- Set metatable to get our class methods
	setmetatable(nImgLib, ImageLibrary_mt)

	nImgLib.images = {}

	nImgLib.imageSets = {}

	nImgLib.count = 0

	return nImgLib

end

function ImageLibrary:setFilters(filter1, filter2)

	for k,v in pairs(self.images) do
		if v ~= nil and type(v) == 'userdata' then v:setFilter(filter1, filter2)
		elseif v ~= nil and type(v) == 'table' then v.img:setFilter(filter1, filter2)
		end
	end

end

function ImageLibrary:loadImageFile(filePath, useSpb)

	if self.images == nil then self.images = {} end

	self.count = self.count or 0

	-- Already loaded.  Tank.
	if self.images[filePath] ~= nil then return self.images[filePath].img end


	local tempImg = love.graphics.newImage(filePath)

	if tempImg ~= nil then 
		self.images[filePath] = {}
		self.images[filePath].ref = 0
		self.images[filePath].img = tempImg
		self.count = self.count + 1
		if(useSpb) then
			local spbSize = (vpWidth * vpHeight * 4) / (pixelsPerUnit * pixelsPerUnit)
			self.images[filePath].spb = love.graphics.newSpriteBatch(tempImg, spbSize)
		end
	end

	return self.images[filePath].img

end

function ImageLibrary:loadImageSet(handle)

	if (handle == nil) then return false end

	if self.images == nil then self.images = {} end
	if self.imageSets == nil then self.imageSets = {} end

	-- Already loaded.  Tank.
	if self.imageSets[handle] ~= nil then return true end

	self.imageSets[handle] = {}

	--print(handle)

	local quadFile = 'resource/images/'..handle..'.xml'

	local quadList = {}

	local xmlTable = xmlLoadFile(quadFile)

	local fileName = xmlTable[1].xarg.imgSrc

	--local tempImg = love.graphics.newImage(fileName)
	local tempImg = self:loadImageFile(fileName)

	if (tempImg ~= nil) then 
		self.imageSets[handle].fileName = fileName
		self.images[fileName].ref = self.images[fileName].ref + 1
	end

	for qk,qv in pairs(xmlTable[1]) do
		if(qv.label == 'quad') then
			local thisName = qv.xarg.name
			local thisId = tonumber(qv.xarg.id) or qv.xarg.id
			local qe = {}
			qe.w = tonumber(qv.xarg.width)
			qe.h = tonumber(qv.xarg.height)
			qe.quad = love.graphics.newQuad(qv.xarg.x, qv.xarg.y, qv.xarg.width, qv.xarg.height, tempImg:getWidth(), tempImg:getHeight())
			quadList[thisName] = qe
			quadList[thisId] = qe
		elseif(qv.label == 'alias') then
			local thisName = qv.xarg.name
			local thisId = qv.xarg.id
			local aliasOf = qv.xarg.aliasOf
			quadList[thisName] = quadList[aliasOf]
			quadList[thisId] = quadList[aliasOf]
		end
	end

	self.imageSets[handle].quads = quadList

	return true

end

function ImageLibrary:getImage(filePath)
--	print('Attempting to retrieve "'..filePath..'" ('..type(self.images[filePath].img)..')')
--	if(self.images[filePath].img == nil) then print('\t..which is nil.') end 
	return self.images[filePath].img
end

function ImageLibrary:getSpb(filePath)
	return self.images[filePath].spb
end

function ImageLibrary:getQuad(set,quad)
	return self.imageSets[set].quads[quad].quad
end

function ImageLibrary:getDim(set,quad)
	--print('getDim:'..set..','..quad)
	return self.imageSets[set].quads[quad].w, self.imageSets[set].quads[quad].h
end

function ImageLibrary:getq(fullId)

	--print('getq:'..fullId)

	local a,b,set,quad = string.find(fullId, '^(.+)%.(.+)$')

	quad = tonumber(quad) or quad

	return self.images[self.imageSets[set].fileName].img, self.imageSets[set].quads[quad].quad

end
