--[[
Copyright 2014 MoikMellah

This file is part of Bits & Bots.

Project SYPHA is free software: you can redistribute it and/or modify it under the terms
of the ZLIB License.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the ZLIB License for more details.

You should have received a copy of the ZLIB License along with
Bits & Bots. If not, see http://zlib.net/zlib_license.html
]]--

require('utils.xml')
require('classes.MapObject')

-- Table declaration for class
LayerObject = {}

-- Metatable for class (to make LayerObject table the default lookup for class methods)
LayerObject_mt = {}
LayerObject_mt.__index = LayerObject

-- Might deprecate the constructor in favor of a global class 'init()' method.

-- Constructor
function LayerObject.new(xmlTable, tileSet)

	if((xmlTable == nil) or (type(xmlTable) ~= 'table')) then return nil end

	-- Make copy from template if provided, or default template if not
	local nLayObj = {}

	-- Set metatable to get our class methods
	setmetatable(nLayObj, LayerObject_mt)

	nLayObj.tileSet = tileSet

	--nLayObj.width = xmlTable.xarg.width
	--nLayObj.height = xmlTable.xarg.height
	nLayObj.hidden = 0
	nLayObj.data = {}

	for m,n in pairs(xmlTable) do
		if (n.label == 'tile') then
			nLayObj.data[#nLayObj.data + 1] = tonumber(n.xarg.gid) or 1
		end
	end

	nLayObj:initCanvas()

	nLayObj.camx = 0
	nLayObj.camy = 0
	nLayObj.lzx = 0
	nLayObj.lzy = 0

	--[[
		Should we process things here, or in an overall Library class?
		..Library makes more sense, so we can have centralized storage
		of tilesets (e.g. local instance of ImageLibrary).
	]]--

	return nLayObj

end

--[[

	..Sigh.  Reorganizing again.  Todo:

	- Figure out how terrain should be handled (tile properties of main layer, or separate layer?)
		- Tile properties is cleaner, but we'd need additional properties to put the 'collision'
			attribute into each tile.  Then, when testing collisions, grab tiles[layer.data[pos]\].coll.
		- Separate layer would allow for e.g. invisible passages using the same tile as surroundings,
			but we'd still need to either track those gid's (ugh) or use properties as above.

	- Improve tileset processing.  If we use tile properties, we can't make the assumption that child
		element '1' of the tileset node is always the image source.  (Probably shouldn't anyway.)

	Basically, this is messy.  Fix it.

]]--

function LayerObject:initCanvas()
	
	self.ctx = love.graphics.newCanvas(vpWidth * 2,vpHeight * 2)
	self.ctx:setFilter('linear', 'nearest')
	self.dirty = true

end

--[[

	**TODO:	- Update to handle multiple layers.  Currently assumes just one, which is
		bad practice.

		- Include checks for the 'visible' attribute of the layer; don't render if
			visible == 'no'.

]]--

function LayerObject:render()

--	print(self.name .. ':render()')

	local lzx = self.lzx
	local lzy = self.lzy

	-- Fetch original canvas
	local oldCtx = love.graphics.getCanvas()

	-- Grab render operations
	love.graphics.setCanvas(self.ctx)

	--self.ctx:clear()
	love.graphics.clear()

	local imgs = {}
	local spbs = {}

	local aMap_width = self.width
	local aMap_height = self.height
	local aMap_tileArray = self.data

	local stx = 0
	local sty = 0

	local limitx = vpWidth / pixels(1)
	local limity = vpHeight / pixels(1)

	if ((lzx - limitx) > 0) then stx = lzx - limitx end
	if ((lzy - limity) > 0) then sty = lzy - limity end

	local edx = aMap_width - 1
	local edy = aMap_height - 1
	if ((lzx + limitx - 1) < edx) then edx = lzx + limitx - 1 end
	if ((lzy + limity - 1) < edy) then edy = lzy + limity - 1 end

	love.graphics.push()

	local xCtr = 0
	local yCtr = 0

	local thisIndex = 0
	local thisTileId = 0
	local thisTile = nil
	local thisX = 0
	local thisY = 0
	local rowPos = 0

	-- Aaaaaand.. RENDER!
	for yCtr = sty,edy do

		rowPos = yCtr * aMap_width
		thisY = (yCtr - (lzy - limity)) * pixelsPerUnit

		for xCtr = stx,edx do
			thisIndex = (rowPos + xCtr) + 1
			thisTileId = self.data[thisIndex]
			if(thisTileId ~= 0) then
				--print('Rendering: pos('..thisIndex..'); tileId('..thisTileId..')')
				thisTile = self.tileSet[thisTileId]
				if(imgs[thisTile.imgPath] == nil) then imgs[thisTile.imgPath] = ImageLibrary:getImage(thisTile.imgPath) end
				if(spbs[thisTile.imgPath] == nil) then spbs[thisTile.imgPath] = ImageLibrary:getSpb(thisTile.imgPath) end
				thisX = (xCtr - (lzx - limitx)) * pixelsPerUnit
				if(spbs[thisTile.imgPath]) then
					if(love.v == 8) then
						spbs[thisTile.imgPath]:addq(thisTile.quad, thisX, thisY, 0, 1, 1, 0)
					else
						spbs[thisTile.imgPath]:add(thisTile.quad, thisX, thisY, 0, 1, 1, 0)
					end
				else
					love.graphics.draw(imgs[thisTile.imgPath], thisTile.quad, thisX, thisY, 0, 1, 1, 0, 0)
				end
			end
		end

	end

	for k,v in pairs(spbs) do

		love.graphics.draw(v, 0, 0, 0, 1, 1, 0, 0)

		v:clear()

	end

	-- Release rendering back to the screen.
	love.graphics.pop()
	love.graphics.setCanvas(oldCtx)
	self.dirty = false

end

function LayerObject:getTileId(x, y)

	local aMap_width = tonumber(self.width)
	local aMap_height = tonumber(self.height)
	if((x < 0) or (x > aMap_width) or (y < 0) or (y > aMap_height)) then return nil end
	local tilePos = ((y * aMap_width) + x) + 1
	local tileIndex = self.data[tilePos]

	return tileId

end

function LayerObject:draw(camera_X, camera_Y)

	--print(self.name .. ':draw()')

	local camX = camera_X / pixelsPerUnit
	local camY = camera_Y / pixelsPerUnit

	local camDiffX = math.abs(camX - self.lzx)
	local camDiffY = math.abs(camY - self.lzy)

	local limitx = vpWidth / pixels(1)
	local limity = vpHeight / pixels(1)

	if ((camDiffX >= (limitx/2)) or (camDiffY >= (limity/2))) then
		--print('lName:'..self.name..';Cam:'..camDiffX..','..camDiffY..';LZ:'..self.lzx..','..self.lzy)
		self.lzx = math.floor(camX / (limitx/2) + .5) * limitx / 2
		self.lzy = math.ceil(math.floor(camY / (limity/2)) * (limity/2))
		self.dirty = true
	end

	if (self.dirty == true) then self:render() end

	local bq = love.graphics.newQuad(pixels((camX-self.lzx)+(limitx/2)),pixels((camY-self.lzy)+(limity/2)),vpWidth,vpHeight,vpWidth*2,vpHeight*2)

	love.graphics.draw(self.ctx, bq, 0, 0, 0, 1, 1, 0, 0)

end
