--[[
Copyright 2014 MoikMellah

This file is part of Bits & Bots.

Project SYPHA is free software: you can redistribute it and/or modify it under the terms
of the ZLIB License.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the ZLIB License for more details.

You should have received a copy of the ZLIB License along with
Bits & Bots. If not, see http://zlib.net/zlib_license.html
]]--

require('utils.xml')
require('classes.MapObject')

-- Table declaration for class
MapLibrary = {}

-- Metatable for class (to make MapLibrary table the default lookup for class methods)
MapLibrary_mt = {}
MapLibrary_mt.__index = MapLibrary

-- Might deprecate the constructor in favor of a global class 'init()' method.

-- Constructor
function MapLibrary.new()

	-- Make copy from template if provided, or default template if not
	local nMapLib = {}

	nMapLib.maps = {}
	nMapLib.activeMap = ''

	nMapLib.images = ImageLibrary.new()

	-- Set metatable to get our class methods
	setmetatable(nMapLib, MapLibrary_mt)

	nMapLib:initCanvas()

	nMapLib.camx = 0
	nMapLib.camy = 0
	nMapLib.lzx = 0
	nMapLib.lzy = 0

	--[[
		Should we process things here, or in an overall Library class?
		..Library makes more sense, so we can have centralized storage
		of tilesets (e.g. local instance of ImageLibrary).
	]]--

	return nMapLib

end

--[[

	..Sigh.  Reorganizing again.  Todo:

	- Figure out how terrain should be handled (tile properties of main layer, or separate layer?)
		- Tile properties is cleaner, but we'd need additional properties to put the 'collision'
			attribute into each tile.  Then, when testing collisions, grab tiles[layer.data[pos]\].coll.
		- Separate layer would allow for e.g. invisible passages using the same tile as surroundings,
			but we'd still need to either track those gid's (ugh) or use properties as above.

	- Improve tileset processing.  If we use tile properties, we can't make the assumption that child
		element '1' of the tileset node is always the image source.  (Probably shouldn't anyway.)

	Basically, this is messy.  Fix it.

]]--

function MapLibrary:loadMap(mapName,x,y)

	local thisMap

	local xmlTable

	if (self.maps[mapName] == nil) then
		xmlTable = xmlLoadFile(mapName)
		if (xmlTable == nil) then return false end
	else
		return true
	end

	thisMap = {}
	thisMap.layers = {}
	thisMap.width = 0
	thisMap.height = 0
	thisMap.tileWidth = 0
	thisMap.tileHeight = 0

	for k,v in pairs(xmlTable) do
		if((type(v) == 'table') and (v.label == 'map')) then

			thisMap.width = v.xarg.width
			thisMap.height = v.xarg.height
			thisMap.tileWidth = v.xarg.tilewidth
			thisMap.tileHeight = v.xarg.tileheight
			thisMap.tiles = {}

			for l,u in pairs(v) do
				if ((type(u) == 'table') and (u.label == 'tileset')) then
					local imgPath = u[1].xarg.source
					local firstGid = u.xarg.firstgid
					local a,b,imgTag = string.find(imgPath, '.*/([^/]+)$')
					local img = ImageLibrary:loadImageFile(imgTag, 'resource/maps/'..imgPath)
					local iw = img:getWidth()
					local ih = img:getHeight()
					local tw = thisMap.tileWidth
					local th = thisMap.tileHeight
					for c = 1,((iw * ih)/(tw * th)) do
						
						local qx = (c-1) % (iw / tw)
						local qy = math.floor((c-1) / (iw / tw))

						thisMap.tiles[c + firstGid - 1] = {}
						thisMap.tiles[c + firstGid - 1].imgTag = imgTag
						thisMap.tiles[c + firstGid - 1].quad = 
							love.graphics.newQuad(qx*tw, qy*th, tw, th, iw, ih)

					end

					-- Process per-tile properties
					for cn = 1,#u do
						if((type(u[cn]) == 'table') and (u[cn].label == 'tile')) then
							local tid = (tonumber(u[cn].xarg.id) or 1) + 1
							local tcoll = tonumber(u[cn][1][1].xarg.value) or 0
							thisMap.tiles[tid].coll = tcoll
						end
					end
				elseif ((type(u) == 'table') and (u.label == 'layer')) then

					local newLayer = {}
					newLayer.width = u.xarg.width
					newLayer.height = u.xarg.height
					newLayer.hidden = 0
					newLayer.data = {}

					for m,n in pairs(u[1]) do
						if (n.label == 'tile') then
							newLayer.data[#newLayer.data + 1] = tonumber(n.xarg.gid) or 1
						end
					end

					thisMap.layers[#thisMap.layers + 1] = newLayer

				end
			end

		end

	end

	self.maps[mapName] = thisMap

	if (self.maps[mapName] == nil) then return nil end

	self.activeMap = mapName

	self.dirty = true

end

function MapLibrary:initCanvas()
	
	self.ctx = love.graphics.newCanvas(640,480)
	self.ctx:setFilter('linear', 'nearest')
	self.dirty = true

end

--[[

	**TODO:	- Update to handle multiple layers.  Currently assumes just one, which is
		bad practice.

		- Include checks for the 'visible' attribute of the layer; don't render if
			visible == 'no'.

]]--

function MapLibrary:render()

	local lzx = self.lzx
	local lzy = self.lzy

	local sMap = self.activeMap

	if(self.maps[sMap] == nil) then return nil end

	-- Fetch original canvas
	local oldCtx = love.graphics.getCanvas()

	-- Grab render operations
	love.graphics.setCanvas(self.ctx)

	self.ctx:clear()

	local imgs = {}

	local aMap_width = self.maps[sMap].width
	local aMap_height = self.maps[sMap].height
	local aMap_tileArray = self.maps[sMap].layers[1].data

	local stx = 0
	local sty = 0
	if ((lzx - 20) > 0) then stx = lzx - 20 end
	if ((lzy - 15) > 0) then sty = lzy - 15 end

	local edx = aMap_width - 1
	local edy = aMap_height - 1
	if ((lzx + 19) < edx) then edx = lzx + 19 end
	if ((lzy + 14) < edy) then edy = lzy + 14 end

	love.graphics.push()

	local xCtr = 0
	local yCtr = 0

	-- Aaaaaand.. RENDER!
	for yCtr = sty,edy do

		for xCtr = stx,edx do
			local thisIndex = ((yCtr * aMap_width) + xCtr) + 1
			local thisTileId = aMap_tileArray[thisIndex]
			local thisTile = self.maps[sMap].tiles[thisTileId]
			if(imgs[thisTile.imgTag] == nil) then imgs[thisTile.imgTag] = ImageLibrary:getImage(thisTile.imgTag) end
			local thisX = (xCtr - (lzx - 20)) * 16
			local thisY = (yCtr - (lzy - 15)) * 16
			love.graphics.draw(imgs[thisTile.imgTag], thisTile.quad, thisX, thisY, 0, 1, 1, 0, 0)
		end

	end

	-- Release rendering back to the screen.
	love.graphics.pop()
	love.graphics.setCanvas(oldCtx)
	self.dirty = false

end

function MapLibrary:getTile(x, y)

	local aMap_width = tonumber(self.maps[self.activeMap].width)
	local aMap_height = tonumber(self.maps[self.activeMap].height)
	if((x < 0) or (x > aMap_width) or (y < 0) or (y > aMap_height)) then return nil end
	local tilePos = ((y * aMap_width) + x) + 1
	local tileIndex = self.maps[self.activeMap].layers[1].data[tilePos]
	local tile = self.maps[self.activeMap].tiles[tileIndex]

	return tile

end

function MapLibrary:draw(camX, camY)

	local camDiffX = math.abs(camX - self.lzx)
	local camDiffY = math.abs(camY - self.lzy)

	if ((camDiffX >= 10) or (camDiffY >= 7.5)) then
		self.lzx = math.floor(camX / 10 + .5) * 10
		self.lzy = math.ceil(math.floor(camY / 7.5) * 7.5)
		self.dirty = true
	end

	if (self.dirty == true) then self:render() end

	local bq = love.graphics.newQuad(pixels((camX-self.lzx)+10),pixels((camY-self.lzy)+7.5),pixels(20),pixels(15),pixels(40),pixels(30))

	love.graphics.draw(self.ctx, bq, 0, 0, 0, 1, 1, 0, 0)

end
