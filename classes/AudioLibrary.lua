--[[
Copyright 2014 MoikMellah

This file is part of Bits & Bots.

Project SYPHA is free software: you can redistribute it and/or modify it under the terms
of the ZLIB License.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the ZLIB License for more details.

You should have received a copy of the ZLIB License along with
Bits & Bots. If not, see http://zlib.net/zlib_license.html
]]--

require('utils.xml')

-- Table declaration for class
AudioLibrary = {}

-- Metatable for class (to make AudioLibrary table the default lookup for class methods)
AudioLibrary_mt = {}
AudioLibrary_mt.__index = AudioLibrary

AudioLibrary.cloneLimit = 10

-- Constructor
function AudioLibrary.new()

	-- Make copy from template if provided, or default template if not
	local nImgLib = {}

	-- Set metatable to get our class methods
	setmetatable(nImgLib, AudioLibrary_mt)

	nImgLib.sounds = {}

	--nImgLib.imageSets = {}

	nImgLib.count = 0

	return nImgLib

end

function AudioLibrary:loadAudioFile(filePath, instLimit)

	local iLimit = instLimit or 10

	local sType = 'static'

	if(iLimit == 1) then
		sType = 'stream'
	end

	if self.sounds == nil then 
		self.sounds = {} 
	end

	self.count = self.count or 0

	-- Already loaded.  Tank.
	if self.sounds[filePath] ~= nil then return self.sounds[filePath].source end

	local tempSnd = love.audio.newSource('resource/'..filePath, sType)

	if tempSnd ~= nil then 
		self.sounds[filePath] = {}
		self.sounds[filePath].source = tempSnd
		self.sounds[filePath].inst = {}
		self.sounds[filePath].limit = iLimit
		self.sounds[filePath].inst[1] = self:clone(tempSnd)
		--for i = 1,iLimit do self.sounds[filePath].inst[i] = self:clone(tempSnd) end
	end

	return self.sounds[filePath].source

end

function AudioLibrary:clone(source)

	if(love._version_minor >= 9) then
		return source:clone()
	end

	return source

end

function AudioLibrary:play(filePath, loop)

	local snd = self.sounds[filePath]

	if not(snd) then return nil end

	-- Get first non-playing instance
	local inst = self:findAvailable(filePath)

	if not(inst) then return nil end

	inst:setLooping(loop)

	inst:play()

	return inst

end

function AudioLibrary:findAvailable(filePath)

	local snd = self.sounds[filePath]

	if not(snd) then return nil end

	local inst = nil

	local c = 1

	while c <= #snd.inst and not(inst) do
		if snd.inst[c]:isStopped() then
			inst = snd.inst[c]
		end
		c = c + 1
	end

	if not(inst) and #snd.inst < snd.limit then
		inst = self:clone(snd.source)
		snd.inst[#snd.inst + 1] = inst
	end

	return inst

end

function AudioLibrary:stopAll()

	self.sounds = self.sounds or {}

	for k,v in pairs(self.sounds) do

		for c,i in ipairs(v.inst) do

			i:stop()
			i:rewind()

		end

	end

end

function AudioLibrary:setMaster(vol)

	self.master = vol

	love.audio.setVolume(self.master)

end
