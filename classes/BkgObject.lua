--[[
Copyright 2014 MoikMellah

This file is part of Bits & Bots.

Project SYPHA is free software: you can redistribute it and/or modify it under the terms
of the ZLIB License.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the ZLIB License for more details.

You should have received a copy of the ZLIB License along with
Bits & Bots. If not, see http://zlib.net/zlib_license.html
]]--

-- Member fields template
BkgObjDefaults = {

	x = 0,				-- x position
	y = 0,				-- y position
	visible = 0,			-- rendered?
	xOffset = 0,			-- x offset within buffer
	yOffset = 0,			-- y offset within buffer
	mapSet = 0,			-- which object in environ.buffers[] we reference
	mapIndex = 0,			-- which map within set we reference
	depth = 0,			-- what depth we render at
	imageSet = 0,			-- which imageSet we use for tiles
	xScroll = 0,			-- does the bkg scroll horizontally?
	yScroll = 0,			-- does the bkg scroll vertically?
	bkgNum = 0,			-- which layer this obj controls
	isSub = 0,			-- subscreen?
	luaClass = "BkgObject",		-- Lua class name
	class = "Camera",		-- engine class name
	name = "Camera"			-- object's name

}

BkgObject = {}

BkgObject_mt = {}
BkgObject_mt.__index = BkgObject

function BkgObject.new(template)

	local bTmp = nil
	if type(template) == "table" then
		bTmp = template
	else
		bTmp = BkgObjDefaults
	end

	-- Make copy from template if provided, or default template if not
	local bObj = tableCopy(bTmp)

	-- Set metatable to get our class methods
	setmetatable(bObj, BkgObject_mt)

	if type(bObj.initLogic) == "function" then bObj.currentLogic = coroutine.create(bObj.initLogic) end

	if type(bObj.init) == "number" then bObj.cam = bObj.init bObj.depth = (bObj.init + 1) * 100 end

	return bObj

end

function BkgObject:toString()

	local str = ""

	if self.name ~= nil then str = self.name
	elseif self.class ~= nil then str = self.class
	elseif self.luaClass ~= nil then str = self.luaClass
	else str = "BkgObject" end

	return str

end

function BkgObject:display()

	if self.visible == 1 then

		-- Display background (depth 3, to ensure that it's behind the Objects)
		-- New format: cDisplayTilemap(mapSet, mapIndex, cameraX, cameraY, depth, imageSet)
		--BkgObject.cDisplayTilemap(self.mapSet, self.mapIndex, self.x + self.xOffset, self.y + self.yOffset, self.depth, self.imageSet)
		--print('Drawing layer '..self.myLayer.name)
		self.myLayer:draw(self.x + self.xOffset, self.y + self.yOffset)

	end

end

function BkgObject:mousePos()

	local tempx = 0
	local tempy = 0

	local tsx = 0
	local tsy = 0

	tempx,tempy = InputLibrary.mousePos()

	tsx = tempx - (self.x - vpWidth/2) 
	tsy = tempy - (self.y - vpHeight/2) 

	return tempx,tempy

end
