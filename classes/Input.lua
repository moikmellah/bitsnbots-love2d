--[[
Copyright 2014 MoikMellah

This file is part of Bits & Bots.

Project SYPHA is free software: you can redistribute it and/or modify it under the terms
of the ZLIB License.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the ZLIB License for more details.

You should have received a copy of the ZLIB License along with
Bits & Bots. If not, see http://zlib.net/zlib_license.html
]]--

Input = {}

Input.states = {}
Input.len = {}

love.mousepressed = function(x, y, button)
	Input.states[INPUT_ML] = 1
end

love.mousereleased = function(x, y, button)
	Input.states[INPUT_ML] = 0
end

function Input.cKeyPressed(key)
	Input.states[key] = Input.states[key] or 0
	Input.len[key] = Input.len[key] or 0
	return Input.len[key]
end

function Input:update(dt)
	--if (self.mouseState == 1) then self.mouseLen = self.mouseLen + dt end
	for k,v in pairs(Input.states) do
		if(v == 1) then Input.len[k] = (Input.len[k] or 0) + dt else Input.len[k] = 0 end
	end
end

--[[

	Input.camMouse - get mouse position, adjusted by camera to world coordinates

]]--
Input.camMouse = function (camNum)

	local tempX = 0
	local tempY = 0
	local tmpCam = camNum
	local myCam = nil

	if type(tmpCam) ~= "number" then tmpCam = 1 end

	-- if we haven't fetched our cam obj yet, and it exists, fetch it
	if cams[tmpCam] ~= nil then myCam = cams[tmpCam] end

	-- fetch mouse position
	--tempX,tempY = Input.cMousePos()
	tempX = love.mouse.getX() * vpWidth / love.window.getWidth()
	tempY = love.mouse.getY() * vpHeight / love.window.getHeight()

	-- if we have a camera ref, adjust pos to world coordinates
	if myCam ~= nil then
		tempX = tempX + myCam.x
		tempY = tempY + myCam.y
	end

	return tempX,tempY

end
