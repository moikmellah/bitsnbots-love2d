--[[
Copyright 2014 MoikMellah

This file is part of Bits & Bots.

Project SYPHA is free software: you can redistribute it and/or modify it under the terms
of the ZLIB License.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the ZLIB License for more details.

You should have received a copy of the ZLIB License along with
Bits & Bots. If not, see http://zlib.net/zlib_license.html
]]--

-- Member fields template; used if no other template is given
GameObjDefaults = {

	x = 0,
	y = 0,
	visible = 0,
	isSub = 0,			-- Deprecated; an artifact of old NDS-based versions of the engine
	imageSet = 0,
	imageNum = 0,
	iOfsX = -8,
	iOfsY = -12,
	facing = 1,
	blendfx = {
		fxmode = 0,
		alpha = 1,
		color = {r=1,g=1,b=1,a=1}
	},
	label = {
		visible = 0,	-- visibility
		x = 0,		-- offset from obj pos
		y = 0,		-- offset from obj pos
		fontSet = 0,	-- imageSet used to store glyphs
		iMode = 0,	-- blending mode (0-3 only)
		hMode = 0,	-- horizontal alignment mode (0-3 only)
		alpha = 1,	-- alpha value for blending (0.0 to 1.0)
		color = {r=1,g=1,b=1,a=1}	-- RGBA; A is alpha value for color blending, though usage depends on iMode
	},
	prim = {
		visible = 0,	-- visibility
		x = 0,		-- offset from obj pos
		y = 0,		-- offset from obj pos
		width = 0,
		height = 0,
		color = 0
	},
	cam = 1,			-- Background to which we're affine (determines rendering coordinates)
	luaClass = "GameObject",
	class = "GameObject",
	name = "Object"

}

-- Table declaration for class
GameObject = {}

-- Metatable for class (to make GameObject table the default lookup for class methods)
GameObject_mt = {}
GameObject_mt.__index = GameObject

-- Constructor
function GameObject.new(template)

	-- Template ref
	local gTmp = nil

	-- Check if arg is a table; if not, use the default table declared above
	if type(template) == "table" then
		gTmp = template
	else
		gTmp = GameObjDefaults
	end

	-- Make copy from template if provided, or default template if not
	local gObj = tableCopy(gTmp)

	-- Set metatable to get our class methods
	setmetatable(gObj, GameObject_mt)

	-- Set up initial coroutine to be run each cycle
	if type(gObj.initLogic) == "function" then gObj.currentLogic = coroutine.create(gObj.initLogic) end

	if gObj.depth == nil then gObj.depth = 0 end

	-- Return object ref
	return gObj

end

function GameObject:spawn(template, px, py)

	local newObj = GameObject.new(template)

	if px ~= nil then newObj.x = px else newObj.x = self.x end
	if py ~= nil then newObj.y = py else newObj.y = self.y end
	newObj.cam = self.cam
	newObj.depth = self.depth

	objs[#objs + 1] = newObj

	return newObj

end

-- Simple test of class members
function GameObject:whoAmI()

	if self.name ~= nil then print("My name is "..self.name) else print("I am nameless.") end

end

-- ToString method (needs work)
function GameObject:toString()

	local str = ""

	if self.name ~= nil then str = self.name
	elseif self.class ~= nil then str = self.class
	elseif self.luaClass ~= nil then str = self.luaClass
	else str = "GameObject" end

	return str

end

-- Preprocesses a few fields (namely, finds the associated camera obj and calculates offsets)
function GameObject:display()

	local camX = 0
	local camY = 0
	local myCamNum = 0

	-- Fetch self.cam, if it's set
	if type(self.cam) == "number" then myCamNum = self.cam end

	-- Fetch camera, if it exists
	local myCam = cams[myCamNum]

	-- If it's valid..
	if type(myCam) == "table" then

		-- Fetch its coords
		if type(myCam.x) == "number" then camX = myCam.x - (vpWidth / 2) end
		if type(myCam.y) == "number" then camY = myCam.y - (vpHeight / 2) end

	end

--	self.depth = self.y / 10

	local pd = ((myCamNum + 1) * 100) - (50 - self.depth)

	-- Primitive:
	--[[
	if type(self.prim) == "table" and self.prim.visible == 1 then

		local pl = 0
		local pt = 0
		local pr = 0
		local pb = 0
		local sw = 0
		local sh = 0
		
		if self.prim.base == "label" and type(self.label) == "table" then
			if type(self.label.text) ~= "string" then self.label.text = self:toString() end
			sw,sh = Font.cGetStrDim(self.label.fontSet, self.label.text)
			pl = self.x - camX - self.label.x - (sw / 2) - self.prim.width
			pt = self.y - camY - self.label.y - self.prim.height
			pr = pl + sw + (self.prim.width * 2)
			pb = pt + sh + (self.prim.height * 2)
		else
			pl = self.x - camX + self.prim.x
			pt = self.y - camY + self.prim.y
			pr = pl + self.prim.width - 1
			pb = pt + self.prim.height - 1
		end
		BkgResource.cDisplayPrimitive(pl,pt,pr,pb,pd,self.prim.color)
	end
	]]--
	-- Call our C++ sprite rendering routine
	self:draw(camX, camY)

	--if 1 == 1 then return true end

	-- Label rendering
	if type(self.label) == "table" and self.label.visible == 1 then

		-- Initialize critical values
		if type(self.label.text) ~= "string" then self.label.text = self:toString() end
		if type(self.label.iMode) ~= "number" then self.label.iMode = 0 end
		if type(self.label.hMode) ~= "number" then self.label.hMode = 0 end
		if type(self.label.x) ~= "number" then self.label.x = 0 end
		if type(self.label.y) ~= "number" then self.label.y = 0 end
		if type(self.label.w) ~= "number" then self.label.w = 60 end
		if type(self.label.h) ~= "number" then self.label.h = 16 end
		if type(self.label.bkgPadding) ~= "number" then self.label.bkgPadding = 0 end

		-- Display the bkg, if present
		if type(self.label.bkgColor) == "table" then

			-- Rect vars
			local pl = 0
			local pt = 0
			local pr = 0
			local pb = 0
			local sw = self.label.w
			local sh = self.label.h
			local pad = 0

			--sw,sh = Font.cGetStrDim(self.label.fontSet, self.label.text)

			-- Calculate initial bound rect values
			pl = self.x - camX + self.label.x - self.label.bkgPadding
			--pr = pl + sw + (self.label.bkgPadding * 2)
			pr = sw + (self.label.bkgPadding * 2)
			pt = self.y - camY + self.label.y - self.label.bkgPadding
			--pb = pt + sh + (self.label.bkgPadding * 2)
			pb = sh + (self.label.bkgPadding * 2)

			-- Adjust for center/right justification
			--[[
			if self.label.hMode == 1 then
				pl = pl - (sw / 2)
				pr = pr - (sw / 2)
			elseif self.label.hMode == 2 then
				pl = pl - sw
				pr = pr - sw
			end
			]]--

			--BkgResource.cDisplayPrimitive(pl,pt,pr,pb,pd,self.label.bkgColor)
			love.graphics.setColor((self.label.bkgColor.r or 1) * 255, (self.label.bkgColor.g or 1) * 255, 
				(self.label.bkgColor.b or 1) * 255, (self.label.bkgColor.a or 1) * 255)
			love.graphics.rectangle('fill', pl,pt, pr,pb)
			love.graphics.setColor(255,255,255,255)

			--if(self.label.debug == 1) then print('labelRect('..pl..','..pt..','..pr..','..pb..')') end

			if type(self.label.brd) == "table" then
				if self.label.brd.w == nil then self.label.brd.w = 1 end
				local brdw = self.label.brd.w
				--[[
				BkgResource.cDisplayPrimitive(pl - brdw, pt - brdw, pr, pt, pd, self.label.brdColor)
				BkgResource.cDisplayPrimitive(pr, pt - brdw, pr + brdw, pb, pd, self.label.brdColor)
				BkgResource.cDisplayPrimitive(pl, pb, pr + brdw, pb + brdw, pd, self.label.brdColor)
				BkgResource.cDisplayPrimitive(pl - brdw, pt, pl, pb + brdw, pd, self.label.brdColor)
				]]--
				love.graphics.setColor((self.label.brd.r or 1) * 255, (self.label.brd.g or 1) * 255, 
					(self.label.brd.b or 1) * 255, (self.label.brd.a or 1) * 255)
				love.graphics.setLineWidth(brdw)
				love.graphics.setLineStyle('rough')
				love.graphics.rectangle('line', pl-(brdw/2),pt-(brdw/2), pr+brdw,pb+brdw)
				--love.graphics.rectangle('line', pl,pt-brdw, pr+brdw,pb+brdw)
				love.graphics.setColor(255,255,255,255)
				
				--if(self.label.debug == 1) then print('bkgRect('..pl-brdw..','..pt-brdw..','..pr+(brdw*2)..','..pb+(brdw*2)..')') end
			end
		end

		-- Display the string
--		Font.cDisplayString(self.label.fontSet, self.label.text, self.x - camX + self.label.x,
--			self.y - camY + self.label.y, pd, self.label.iMode + (self.label.hMode * 4), self.label.color, self.label.alpha)
		self.label.color = self.label.color or {}
		self.label.yofs = self.label.yofs or 0
		self.label.font = self.label.font or 'default'

		love.graphics.setFont(fontLib[self.label.font])		
		love.graphics.setColor((self.label.color.r or 1) * 255, (self.label.color.g or 1) * 255, 
			(self.label.color.b or 1) * 255, (self.label.color.a or 1) * 255)
		love.graphics.setScissor(self.x - camX + self.label.x, self.y - camY + self.label.y, self.label.w, self.label.h)
		love.graphics.printf(self.label.text, math.floor(self.x - camX + self.label.x), math.floor(self.y - camY + self.label.y) + self.label.yofs, self.label.w or 0, self.label.align or 'center')
		love.graphics.setScissor()
		love.graphics.setColor(255, 255, 255, 255)

	end

end

--[[

	dragonMoveTest() - Moves/resizes the dragon, depending on user input.  Will be moved.

]]--
function dragonMoveTest(self)

	--Useless 'while' loop counter
	local z = 0

	--Animation lookup table
	local imgs = {}

	imgs[1] = 0
	imgs[2] = 1
	imgs[3] = 2
	imgs[4] = 1

	--Current image number
	local imgnum = 0

	--Animation timer
	local cFrame = 0

	--Animation threshold
	local tFrame = 5

	--Set our scaling to 1
	self.scaleX = 1
	self.scaleY = 1

	--Loop forever, basically
	while z == 0 do

		--Right Arrow held?
		if Input.cKeyLen(INPUT_DR) > 0 then
			self.hFlip = 0		--Set right-facing
			self.x = self.x + 1.5	--Move right
		end

		--Left Arrow held?
		if Input.cKeyLen(INPUT_DL) > 0 then
			self.hFlip = 1		--Set left-facing
			self.x = self.x - 1.5	--Move left
		end

		-- If 's' key pressed, set camera to follow dragon (this obj)
		if Input.cKeyLen(INPUT_BA) > 0 then
			followObj = self
		end

		-- If space key pressed, set camera to follow robotObj (assuming it's checked in already)
		if Input.cKeyLen(INPUT_BB) > 0 then
			followObj = robotObj
		end

		--Up Arrow held longer than 1/2 second?
		if Input.cKeyLen(INPUT_DU) > 16 then
			self.scaleX = self.scaleX + 0.05	--Increase horizontal scaling
			self.scaleY = self.scaleX		--Set vertical scaling = horizontal
		end

		--Down Arrow held longer than 1/2 second?
		if Input.cKeyLen(INPUT_DD) > 16 then
			self.scaleX = self.scaleX - 0.05	--Decrease horizontal scaling
			self.scaleY = self.scaleX		--Set vertical scaling = horizontal
		end

		--Increment animation timer
		cFrame = cFrame + 1

		--Is timer past threshold?
		if cFrame > tFrame then
			cFrame = 0				--Reset the timer
			imgnum = (imgnum + 1) % 4		--Increment the image index
			self.imageNum = imgs[imgnum + 1]	--Update our image index with value from lookup table
		end

		--Return execution to main process
		coroutine.yield()

	end

-- FIN
end

--[[

	moveRobot() - function to move the Robot in a square pattern and animate it.

]]--
function moveRobot(self)

	-- Declare a global reference to this object, for camera following (see robotFollow() below)
	robotObj = self

	-- table of animation frames to rotate through, moving left or right
	local imgs_lr = {}
	imgs_lr[1] = 0
	imgs_lr[2] = 1
	imgs_lr[3] = 2
	imgs_lr[4] = 3

	-- table of frames to rotate through, moving upward
	local imgs_up = {}
	imgs_up[1] = 11
	imgs_up[2] = 10
	imgs_up[3] = 9
	imgs_up[4] = 8

	-- table of frames to rotate through, moving downward
	local imgs_down = {}
	imgs_down[1] = 4
	imgs_down[2] = 5
	imgs_down[3] = 6
	imgs_down[4] = 7

	-- reference to whichever table above is appropriate
	local imgs = nil

	-- current image number
	local imgnum = 0

	-- amount to move robot by per cycle
	local vel = 1

	-- counters
	local z = 0
	local cFrame = 0

	-- Outer loop
	while z == 0 do

		-- set animation table to left/right
		imgs = imgs_lr
		-- initialize image number
		self.imageNum = imgs[imgnum + 1]
		-- set mirroring on
		self.hFlip = 1
		-- set image offsets appropriately
		self.iOfsX, self.iOfsY = -8, -12

		-- Move right until we hit a specific spot
		while self.x < 190 do
			self.x = self.x + vel

			-- iterate our animation counter
			cFrame = cFrame + 1

			-- if we've hit X cycles, flip to next image
			if cFrame > 2 then
				cFrame = 0
				imgnum = (imgnum + 1) % 4
				self.imageNum = imgs[imgnum + 1]
			end
			-- return to main execution
			coroutine.yield()
		end

		-- setup for moving down
		imgs = imgs_down
		self.imageNum = imgs[imgnum + 1]
		self.hFlip = 0
		self.iOfsX, self.iOfsY = -12, -12

		-- Move down
		while self.y < 180 do
			self.y = self.y + vel
			cFrame = cFrame + 1
			if cFrame > 2 then
				cFrame = 0
				imgnum = (imgnum + 1) % 4
				self.imageNum = imgs[imgnum + 1]
			end
			coroutine.yield()
		end

		-- setup for moving left
		imgs = imgs_lr
		self.imageNum = imgs[imgnum + 1]
		self.hFlip = 0
		self.iOfsX, self.iOfsY = -8, -12

		-- Move left
		while self.x > 22 do
			self.x = self.x - vel
			cFrame = cFrame + 1
			if cFrame > 2 then
				cFrame = 0
				imgnum = (imgnum + 1) % 4
				self.imageNum = imgs[imgnum + 1]
			end
			coroutine.yield()
		end

		-- setup for moving upward
		imgs = imgs_up
		self.imageNum = imgs[imgnum + 1]
		self.hFlip = 0
		self.iOfsX, self.iOfsY = -12, -12

		-- Move up
		while self.y > 12 do
			self.y = self.y - vel
			cFrame = cFrame + 1
			if cFrame > 2 then
				cFrame = 0
				imgnum = (imgnum + 1) % 4
				self.imageNum = imgs[imgnum + 1]
			end
			coroutine.yield()
		end

		botCheck[self.botId] = true
		self.nextLogic = Robot.waitCmd
		return nil

	end

end

--[[

	goalRotate() - simple rotation between frames, no movement

]]--
function goalRotate(self,dtA)

	local dt = dtA
	local dummy = nil

	local cFrame = 0
	local z = 0
	local imgnum = 0

	while z == 0 do
		cFrame = cFrame + dt
		if cFrame > .19 then
			cFrame = 0
			imgnum = (imgnum + 1) % 4
			self.quad = imgnum + 1
		end
		dummy,dt = coroutine.yield()
	end

end

--[[

	goalFollow() - copy of above, with instructions to move to mouse pos upon click event

]]--
function goalFollow(self)

	local cFrame = 0
	local z = 0
	local imgnum = 0
	local tempX = 0
	local tempY = 0
	local myCam = nil
	local myCamNum = 0

	-- if our obj has a camera number set, fetch it
	if type(self.cam) == "number" then myCamNum = self.cam end

	-- outer loop
	while z == 0 do

		-- if we haven't fetched our cam obj yet, and it exists, fetch it
		if myCam == nil and cams[myCamNum] ~= nil then myCam = cams[myCamNum] end

		-- on mouse left click..
		if Input.cKeyPressed(INPUT_ML) > 0 then

			-- fetch mouse position
			tempX,tempY = Input.camMouse(self.cam) --cMousePos()

			-- if we have a camera ref, adjust pos to world coordinates
			if myCam ~= nil then
				tempX = tempX + myCam.x
				tempY = tempY + myCam.y
			end

			-- set obj coords to (adjusted) mouse pos
			self.x = tempX
			self.y = tempY
		end

		-- rotate image frame
		cFrame = cFrame + 1
		if cFrame > 2 then
			cFrame = 0
			imgnum = (imgnum + 1) % 4
			self.imageNum = imgnum
		end

		-- return to main execution
		coroutine.yield()
	end

end

--[[

	robotFollow() - logic for camera.  Follow whichever obj is referenced by global 'followObj' (which is set
		by dragon's logic above)

]]--
function robotFollow(self)

	-- Loop forever, basically
	while 1 == 1 do

		-- If followObj has been set..
		if type(followObj) == "table" then

			-- Turn on camera visibility
			self.visible = 1

			-- Center camera on followed object
			self.x = followObj.x - 160
			self.y = followObj.y - 120

		end

		-- return to main execution
		coroutine.yield()

	end

end

--[[

	getLabelRect() - returns label rect

]]--

function GameObject:getLabelRect()

	if type(self.label) ~= "table" then self.label = { visible = 0 } end

	if self.label.visible ~= 1 then return 0,0,0,0 end

	-- Initialize critical values
	if type(self.label.text) ~= "string" then self.label.text = self:toString() end
	if type(self.label.x) ~= "number" then self.label.x = 0 end
	if type(self.label.y) ~= "number" then self.label.y = 0 end
	if type(self.label.w) ~= "number" then self.label.w = 0 end
	if type(self.label.h) ~= "number" then self.label.h = 0 end
	if type(self.label.bkgPadding) ~= "number" then self.label.bkgPadding = 0 end

	-- Rect vars
	local pl = 0
	local pt = 0
	local pr = 0
	local pb = 0
	local sw = 0
	local sh = 0
	local pad = 0

	sw,sh = self.label.w,self.label.h--Font.cGetStrDim(self.label.fontSet, self.label.text)

	-- Calculate initial bound rect values
	pl = self.x + self.label.x - self.label.bkgPadding
	pr = pl + sw + (self.label.bkgPadding * 2)
	pt = self.y + self.label.y - self.label.bkgPadding
	pb = pt + sh + (self.label.bkgPadding * 2)

	--[[ Adjust for center/right justification
	if self.label.hMode == 1 then
		pl = pl - (sw / 2)
		pr = pr - (sw / 2)
	elseif self.label.hMode == 2 then
		pl = pl - sw
		pr = pr - sw
	end
	]]--
	return pl,pt,pr,pb

end

--[[

	labelCollision() - tests for collision of mouse with label (screen-adjusted)

]]--

function GameObject:labelCollision(tx, ty)

	--if 1 == 1 then return false end

	local pl = 0
	local pt = 0
	local pr = 0
	local pb = 0

	pl,pt,pr,pb = self:getLabelRect()

	return (tx >= pl and tx <= pr and ty >= pt and ty <= pb)

end

--[[

	getCamCoords() - fetches offsets of our viewport

]]--

function GameObject:getCamCoords()

	local camX = 0
	local camY = 0
	local myCamNum = 0

	-- Fetch self.cam, if it's set
	if type(self.cam) == "number" then myCamNum = self.cam end

	-- Fetch camera, if it exists
	local myCam = cams[myCamNum]

	-- If it's valid..
	if type(myCam) == "table" then

		-- Fetch its coords
		if type(myCam.x) == "number" then camX = myCam.x end
		if type(myCam.y) == "number" then camY = myCam.y end

	end

	return camX,camY	

end

function GameObject:draw(camx, camy)

	self.facing = self.facing or 1

	if(self.visible ~= 1) then return false end

	local myquad = self.quad

	local qw,qh = ImageLibrary:getDim(self.imageSet, self.quad)

	local sx = 1 --tonumber(myfrm.sx) or 1
	local sy = 1 --tonumber(myfrm.sy) or 1

	local tx1 = self.x - (sx * self.facing * self.iOfsX)
	local tx2 = tx1 + (qw * self.facing)
	local ty1 = self.y - (sy * self.iOfsY)
	local ty2 = ty1 + qh

	local ox1,oy1,ox2,oy2 = math.min(tx1,tx2), math.min(ty1,ty2), math.max(tx1,tx2), math.max(ty1,ty2)
	--local sx1,sy1,sx2,sy2 = pixels(camx - 10), pixels(camy - 7.5), pixels(camx + 10), pixels(camy + 7.5)
	local sx1,sy1,sx2,sy2 = camx, camy, camx + vpWidth, camy + vpHeight

	if (self.debug) then print(self:toString()..':'..oy1..';'..self.y..'/'..self.iOfsY) end

	if((self.visible ~= 0) and (ox1 < sx2) and (sx1 < ox2) and (oy1 < sy2) and (sy1 < oy2)) then

		self.color = self.color or {r=1,g=1,b=1,a=1}

		local img,iq = ImageLibrary:getq(self.imageSet..'.'..myquad)

		love.graphics.setColor(self.color.r * 255, self.color.g * 255, self.color.b * 255, self.color.a * 255)
		love.graphics.draw(img, iq, self.x - camx, self.y - camy, 0, self.facing * sx, sy, tonumber(self.iOfsX), tonumber(self.iOfsY))

	end

	if((self.helper ~= nil) and (myfrm.hzofs == 1)) then
		self.helper:draw(camx, camy)
	end

	if(renderBounds) then

		local x1,y1,x2,y2 = self:getBounds()

		local r,g,b,a = love.graphics.getColor()

		love.graphics.setColor(255,0,0,64)

		love.graphics.rectangle('fill',pixels(x1 - (camx - 10)),pixels(y1 - (camy - 7.5)),pixels(x2-x1),pixels(y2-y1))

		love.graphics.setColor(r,g,b,a)

	end

end

function GameObject:mousePos()

	local tempx,tempy

	tempx,tempy = 0,0

	local camNum = self.cam or 0

	local myCam = cams[camNum]

	if(myCam ~= nil) then
		tempx,tempy = myCam:mousePos()
	else
		tempx,tempy = InputLibrary:mousePos()
	end

	return tempx,tempy

end
