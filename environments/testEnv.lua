--[[
Copyright 2011 MoikMellah

This file is part of Bits & Bots.

Bits & Bots is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
Bits & Bots. If not, see http://www.gnu.org/licenses/
]]--



-- print("Test environment loading.\nProbably want it as functions in a table of some sort, to facilitate load/unload.\n")

maps[1] = {}

maps[1].filePath = "resource/maps/board.tmx"

maps[1].templates = {}

maps[1].templates[1] = {

	--debug = 1,
	init = 1,
	cam = 1,
	x = 20,
	y = 186,
	depth = 5,
	visible = 1,
	imageSet = 'goalBlue',
	quad = 2,
	iOfsX = 16,
	iOfsY = 14,
	name = "Blue Goal",
	initLogic = goalRotate

}

maps[1].templates[2] = {

	init = 1,
	cam = 1,
	x = 188,
	y = 18,
	depth = 5,
	visible = 1,
	imageSet = 'goalRed',
	quad = 2,
	iOfsX = 16,
	iOfsY = 14,
	name = "Red Goal",
	initLogic = goalRotate

}

maps[1].templates[3] = {

	init = 1,
	cam = 1,
	x = 188,
	y = 186,
	depth = 5,
	visible = 1,
	imageSet = 'goalGreen',
	quad = 1,
	iOfsX = 16,
	iOfsY = 14,
	name = "Green Goal",
	initLogic = goalRotate
	--initLogic = goalFollow

}

maps[1].templates[4] = {

	init = 1,
	cam = 1,
	x = 20,
	y = 18,
	depth = 5,
	visible = 1,
	imageSet = 'goalViolet',
	quad = 1,
	iOfsX = 16,
	iOfsY = 14,
	name = "Violet Goal",
	initLogic = goalRotate
	--initLogic = goalFollow

}

maps[1].templates[5] = {

	-- init = 1,
	cam = 1,
	x = 160,
	y = 100,
	visible = 1,
	imageSet = 'botBlue',
	quad = 2,
	botId = 5,
	iOfsX = 16,
	iOfsY = 20,
	name = "Blue Robot",
	initLogic = Robot.retire

}

maps[1].templates[6] = {

	--debug = 1,
	init = 1,
	cam = 1,
	x = 117,
	y = 14,
	visible = 1,
	imageSet = 'botBlue',
	-- imageSet = 17,
	quad = 2,
	iOfsX = 16,
	iOfsY = 20,
	botId = 1,
	moves = {},
	label = {
		visible = 0,
		x = 0,
		y = 0,
		fontSet = 17,
		mode = 2,
		alpha = 1,
		--color = 0x7777ff80
	},
	prim = {
		visible = 0,
		x = -15,
		y = -15,
		width = 29,
		height = 34,
		--color = 0x00000070
	},
	gx = 5,
	gy = 4,
	wx = 1,
	wy = 8,
	name = "Blue Robot",
	initLogic = Robot.setup

}

maps[1].templates[7] = {

	init = 1,
	cam = 1,
	x = 21,
	y = 86,
	visible = 1,
	imageSet = 'botGreen',
	quad = 2,
	iOfsX = 16,
	iOfsY = 20,
	botId = 2,
	moves = {},
	gx = 4,
	gy = 4,
	wx = 8,
	wy = 8,
	name = "Green Robot",
	initLogic = Robot.setup --waitCmd --moveRobot

}

maps[1].templates[8] = {

	init = 1,
	cam = 1,
	x = 93,
	y = 182,
	visible = 1,
	imageSet = 'botRed',
	quad = 2,
	iOfsX = 16,
	iOfsY = 20,
	botId = 3,
	moves = {},
	gx = 4,
	gy = 5,
	wx = 8,
	wy = 1,
	name = "Red Robot",
	initLogic = Robot.setup --moveRobot

}

maps[1].templates[9] = {

	init = 1,
	cam = 1,
	x = 189,
	y = 110,
	visible = 1,
	imageSet = 'botViolet',
	quad = 2,
	iOfsX = 16,
	iOfsY = 20,
	botId = 4,
	moves = {},
	gx = 5,
	gy = 5,
	wx = 1,
	wy = 1,
	name = "Violet Robot",
	initLogic = Robot.setup --moveRobot

}

local btn_incr_x = 32

maps[1].templates[10] = {

	init = 1,
	cam = 1,
	x = 24,
	y = 216,
	visible = 1,
	imageSet = 'buttons',
	quad = 1,
	iOfsX = 10,
	iOfsY = 20,
	fct = 'DIR',
	val = 'U',
	btnId = 1,
	name = "LED Button",
	initLogic = Buttons.waitToggle

}

maps[1].templates[11] = {

	init = 1,
	cam = 1,
	x = maps[1].templates[10].x + btn_incr_x,
	y = maps[1].templates[10].y,
	visible = 1,
	imageSet = 'buttons',
	quad = 1,
	iOfsX = 10,
	iOfsY = 20,
	fct = 'DIR',
	val = 'D',
	btnId = 2,
	name = "LED Button",
	initLogic = Buttons.waitToggle

}

maps[1].templates[12] = {

	init = 1,
	cam = 1,
	x = maps[1].templates[11].x + btn_incr_x,
	y = maps[1].templates[10].y,
	visible = 1,
	imageSet = 'buttons',
	quad = 1,
	iOfsX = 10,
	iOfsY = 20,
	fct = 'DIR',
	val = 'L',
	btnId = 3,
	name = "LED Button",
	initLogic = Buttons.waitToggle

}

maps[1].templates[13] = {

	init = 1,
	cam = 1,
	x = maps[1].templates[12].x + btn_incr_x,
	y = maps[1].templates[10].y,
	visible = 1,
	imageSet = 'buttons',
	quad = 1,
	iOfsX = 10,
	iOfsY = 20,
	fct = 'DIR',
	val = 'R',
	btnId = 4,
	name = "LED Button",
	initLogic = Buttons.waitToggle

}

maps[1].templates[14] = {

	init = 1,
	cam = 1,
	x = maps[1].templates[13].x + btn_incr_x,
	y = maps[1].templates[10].y,
	visible = 1,
	imageSet = 'buttons',
	quad = 1,
	iOfsX = 10,
	iOfsY = 20,
	fct = 'BOT',
	val = 1,
	btnId = 5,
	name = "LED Button",
	initLogic = Buttons.waitToggle

}

maps[1].templates[15] = {

	init = 1,
	cam = 1,
	x = maps[1].templates[14].x + btn_incr_x,
	y = maps[1].templates[10].y,
	visible = 1,
	imageSet = 'buttons',
	quad = 1,
	iOfsX = 10,
	iOfsY = 20,
	fct = 'BOT',
	val = 2,
	btnId = 6,
	name = "LED Button",
	initLogic = Buttons.waitToggle

}

maps[1].templates[16] = {

	init = 1,
	cam = 1,
	x = maps[1].templates[15].x + btn_incr_x,
	y = maps[1].templates[10].y,
	visible = 1,
	imageSet = 'buttons',
	quad = 1,
	iOfsX = 10,
	iOfsY = 20,
	fct = 'BOT',
	val = 3,
	btnId = 7,
	name = "LED Button",
	initLogic = Buttons.waitToggle

}

maps[1].templates[17] = {

	init = 1,
	cam = 1,
	x = maps[1].templates[16].x + btn_incr_x,
	y = maps[1].templates[10].y,
	visible = 1,
	imageSet = 'buttons',
	quad = 1,
	iOfsX = 10,
	iOfsY = 20,
	fct = 'BOT',
	val = 4,
	btnId = 8,
	name = "LED Button",
	initLogic = Buttons.waitToggle

}

maps[1].templates[18] = {

	init = 1,
	cam = 1,
	x = maps[1].templates[17].x + btn_incr_x + 4,
	y = maps[1].templates[10].y,
	visible = 1,
	imageSet = 'buttons',
	quad = 5,
	iOfsX = 10,
	iOfsY = 20,
	name = "EXEC Button",
	initLogic = Buttons.waitExec

}

maps[1].templates[19] = {

	init = 0,
	cam = 1,
	x = 100,
	y = 100,
	visible = 0,
	imageSet = 0,
	-- imageSet = 17,
	quad = 1,
	iOfsX = 0,
	iOfsY = 0,
	label = {
		visible = 1,
		text = "*click*",
		x = -8,
		y = -10,
		w = 20,
		h = 14,
		align = 'left',
		fontSet = 17,
		mode = 0,
		alpha = 1,
		--color = 0xffffff80
	},
--[[	prim = {
		visible = 1,
		x = -15,
		y = -15,
		width = 29,
		height = 34,
		--color = 0x00000070
	}, ]]--
	name = "*Label*",
	initLogic = Buttons.hexFloat

}

maps[1].templates[20] = {

	init = 1,
	cam = 1,
	x = 260,
	y = 75,
	visible = 0,
	imageSet = 0,
	-- imageSet = 17,
	quad = 1,
	iOfsX = 0,
	iOfsY = 0,
	label = {
		visible = 1,
		-- text = "*click*",
		x = -36,
		y = -4,
		w = 72,
		h = 20,
		fontSet = 17,
		iMode = 0,
		align = 'right',
		alpha = 1,
		--color = 0xffffff80
	},
--[[	prim = {
		visible = 1,
		x = -15,
		y = -15,
		width = 29,
		height = 34,
		--color = 0x00000070
	}, ]]--
	name = "HudScore",
	initLogic = Textbox.scoreDisplay

}

maps[1].templates[21] = {

	init = 1,
	cam = 1,
	x = 260,
	y = 75,
	visible = 0,
	imageSet = 0,
	-- imageSet = 17,
	quad = 1,
	iOfsX = 0,
	iOfsY = 0,
	label = {
		visible = 1,
		text = "Time:\nMoves:",
		x = -36,
		y = -4,
		w = 72,
		h = 20,
		align = 'left',
		fontSet = 17,
		iMode = 0,
		hMode = 1,
		alpha = 1,
		--color = 0xffffff80
	},
	name = "HudLogo",
	initLogic = Textbox.logoDisplay

}

maps[1].templates[22] = {

	init = 0,
	cam = 1,
	x = 235,
	y = 40,
	visible = 0,
	imageSet = 0,
	-- imageSet = 17,
	quad = 1,
	iOfsX = 0,
	iOfsY = 0,
	label = {
		visible = 1,
		text = "by MoikMellah",
		x = -10,
		y = -8,
		fontSet = 17,
		iMode = 0,
		hMode = 0,
		alpha = 1,
		--color = 0xffffff80
	},
	name = "HudScore",
	initLogic = Textbox.logoDisplay

}

maps[1].templates[23] = {

	init = 1,
	cam = 1,
	x = 104,
	y = 80,
	depth = -10,
	visible = 0,
	imageSet = 0,
	-- imageSet = 17,
	quad = 1,
	iOfsX = 0,
	iOfsY = 0,
	label = {
		visible = 0,
		-- text = "by MoikMellah",
		x = -70,
		y = 0,
		w = 140,
		h = 60,
		fontSet = 17,
		iMode = 2,
		hMode = 1,
		alpha = 1,
		color = {r=1,g=1,b=1,a=1},
		bkgColor = {r=0x0,g=0,b=0,a=.7},
		brd = {r=.7,g=.7,b=1,a=1,w=2},
		--color = 0xffaaaa80,
		--bkdColor = 0x000000b0, --0xff222260,
		bkgPadding = 10,
		--brdColor = 0xffffff90,
		brdWidth = 1

	},
--	prim = {
--		visible = 0,
--		base = "label",
--		x = -88,
--		y = -8,
--		width = 176,
--		height = 76,
--		--color = 0x00000090
--	},
	name = "WinDisplay",
	initLogic = Textbox.displayWin

}

maps[1].templates[24] = {

	init = 1,
	cam = 1,
	x = 209, --108,
	y = 20,
	xtgt = 108,
	xvel = 16,
	depth = -3,
	visible = 1,
	imageSet = 'logoWords',
	quad = 'BITS',
	iOfsX = 0,
	iOfsY = 0,
	name = "Bits",
	turn = 1,
--	menuLogic = Textbox.logoDisplay,
	initLogic = Textbox.logoDisplay

}

maps[1].templates[25] = {

	init = 1,
	cam = 1,
	x = 245, --152,
	y = 18,
	ytgt = 45,
	yvel = 8,
	depth = -2,
	visible = 1,
	imageSet = 'logoAnd',
	quad = 1,
	iOfsX = 0,
	iOfsY = 0,
	name = "AND",
	turn = 3,
--	menuLogic = Textbox.logoDisplay,
	initLogic = Textbox.logoDisplay

}

maps[1].templates[26] = {

	init = 1,
	cam = 1,
	x = 263, --166,
	y = 20,
	xtgt = 166,
	xvel = -16,
	depth = -1,
	visible = 1,
	imageSet = 'logoWords',
	quad = 2,
	iOfsX = 0,
	iOfsY = 0,
	name = "Bots",
	turn = 2,
--	menuLogic = Textbox.logoDisplay,
	initLogic = Textbox.logoDisplay

}


maps[1].cameras = {}

maps[1].cameras[1] = {

	layerName = 'playField',
	init = 1,
	x = 160,
	y = 120,
	xOffset = 0,
	yOffset = 0,
	mapSet = 0,
	mapIndex = 0,
	imageSet = 3,
	visible = 1,
	initLogic = robotFollow

}


maps[1].init = function ()

	--math.randomseed(os.time())
	--math.randomseed(Input.cGetTicks())

	fadeColor = 255

	botCheck = {}
	for gC = 1,4 do botCheck[gC] = false end
	botWin = {}
	for gC = 1,4 do botWin[gC] = false end

	-- Randomize button positions
	local bC = 0
	btnPosTmp = {}
	btnPos = {}
	for bC = 1,8 do btnPosTmp[bC] = bC - 1 end
	for bC = 1,8 do
		local bI = love.math.random(1,9 - bC)
		btnPos[bC] = btnPosTmp[bI]
		table.remove(btnPosTmp, bI)
		-- print("btnPos["..bC.."] = "..btnPos[bC]) -- Uncomment this to cheat by printing out the table of button positions
	end 

	-- Initialize btnVal table - various button states and values
	btnVal = {}
	btnVal.DIR = { U = 0, D = 0, L = 0, R = 0 }
	btnVal.BOT = { [1] = 0, [2] = 0, [3] = 0, [4] = 0 }
	btnVal.byPos = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0 }

	-- For conversion from decimal to hex
	btnVal.hex = { [10] = "A", [11] = "B", [12] = "C", [13] = "D", [14] = "E", [15] = "F" }

	-- Instantiate grid, and initialize edges and empty space
	grid = {}
	for gC = 0,9 do
		grid[gC] = {}
		if gC == 0 or gC == 9 then
			for gD = 0,9 do
				grid[gC][gD] = 9
			end
		else
			grid[gC][0] = 9
			grid[gC][9] = 9
			for gD = 1,8 do
				grid[gC][gD] = 0
			end
		end
	end

	-- Set initial bot positions
	grid[4][4] = 2
	grid[5][4] = 1
	grid[4][5] = 3
	grid[5][5] = 4

	inputOk = true
	cmdGo = false

	-- Score data
	score = {}
	score.time = 0
	score.points = 0
	score.moves = 0

	AudioLibrary:play(gameMusic, true)

	-- fadeColor = 0x00000090

end

