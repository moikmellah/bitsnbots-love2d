--[[
Copyright 2011 MoikMellah

This file is part of Bits & Bots.

Bits & Bots is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
Bits & Bots. If not, see http://www.gnu.org/licenses/
]]--

--[[

	TitleScreen.lua - load and execute menu screen.  Uses map 2.

]]--

maps[2] = {}

maps[2].filePath = "resource/maps/title.tmx"
maps[2].bkgMusic = "music/menu-skrjablin.ogg"

maps[2].templates = {}

maps[2].templates[1] = {

	init = 1,
	cam = 1,
	x = 160,
	y = 280,
	ytgt = 100,
	yvel = -20*speedAdjust,
	visible = 0,
	imageSet = 0,
	imageNum = 0,
	iOfsX = 0,
	iOfsY = 0,
	turn = 5,
	label = {
		debug = 1,
		visible = 1,
		text = "Play Game",
		x = -60,
		y = 0,
		w = 120,
		h = 12,
		fontSet = 17,
		iMode = 0,
		hMode = 1,
		alpha = 1,
		color = {r=1,g=1,b=1,a=1},
		bkgColor = {r=.14,g=.23,b=.71,a=.38}, --0xff222260,
		bkgPadding = 4,
		brd = {r=1,g=1,b=1,a=1,w=1},
		brdWidth = 1
	},
	name = "MenuPlay",
	sfxthresh = 8,
	sfx = 'sfx/thud.ogg',
	menuLogic = Textbox.menuPlay, --hoverYellow, --logoDisplay
	initLogic = Textbox.waitMyTurn

}

maps[2].templates[2] = {

	init = 1,
	cam = 1,
	x = 160,
	y = 280,
	yvel = -20*speedAdjust,
	ytgt = 130,
	visible = 0,
	imageSet = 0,
	imageNum = 0,
	iOfsX = 0,
	iOfsY = 0,
	turn = 6,
	label = {
		visible = 1,
		text = "Instructions",
		x = -60,
		y = 0,
		w = 120,
		h = 12,
		fontSet = 17,
		iMode = 0,
		hMode = 1,
		alpha = 1,
		color = {r=1,g=1,b=1,a=1},
		bkgColor = {r=.14,g=.23,b=.71,a=.38}, --0xff222260,
		bkgPadding = 4,
		brd = {r=1,g=1,b=1,a=1,w=1},
		brdWidth = 1
	},
	name = "MenuInstr",
	sfxthresh = 8,
	sfx = 'sfx/thud.ogg',
	menuLogic = Textbox.menuInstr, --hoverYellow, --logoDisplay
	initLogic = Textbox.waitMyTurn

}

maps[2].templates[3] = {

	init = 1,
	cam = 1,
	x = 160,
	y = 280,
	yvel = -20*speedAdjust,
	ytgt = 160,
	visible = 0,
	turn = 7,
	imageSet = 0,
	imageNum = 0,
	iOfsX = 0,
	iOfsY = 0,
	label = {
		visible = 1,
		text = "About",
		x = -60,
		y = 0,
		w = 120,
		h = 12,
		yofs = 0,
		fontSet = 17,
		iMode = 0,
		hMode = 1,
		alpha = 1,
		color = {r=1,g=1,b=1,a=1},
		bkgColor = {r=.14,g=.23,b=.71,a=.38}, --0xff222260,
		bkgPadding = 4,
		brd = {r=1,g=1,b=1,a=1,w=1},
		brdWidth = 1
	},
	name = "MenuAbout",
	sfxthresh = 8,
	sfx = 'sfx/thud.ogg',
	menuLogic = Textbox.menuCred, --hoverYellow, --logoDisplay
	initLogic = Textbox.waitMyTurn

}

maps[2].templates[4] = {

	init = 1,
	cam = 1,
	x = 160,
	y = 280,
	yvel = -20*speedAdjust,
	ytgt = 190,
	turn = 8,
	visible = 0,
	imageSet = 0,
	imageNum = 0,
	iOfsX = 0,
	iOfsY = 0,
	label = {
		visible = 1,
		text = "Quit",
		x = -60,
		y = 0,
		w = 120,
		h = 12,
		fontSet = 17,
		iMode = 0,
		hMode = 1,
		alpha = 1,
		color = {r=1,g=1,b=1,a=1},
		bkgColor = {r=.14,g=.23,b=.71,a=.38}, --0xff222260,
		bkgPadding = 4,
		brd = {r=1,g=1,b=1,a=1,w=1},
		brdWidth = 1
	},
	name = "MenuQuit",
	sfxthresh = 8,
	sfx = 'sfx/thud.ogg',
	playMusic = true,
	menuLogic = Textbox.menuQuit, --hoverYellow, --logoDisplay
	initLogic = Textbox.waitMyTurn

}

maps[2].templates[5] = {

	init = 1,
	cam = 1,
	x = -64,
	y = 40,
	xtgt = 108,
	xvel = 16*speedAdjust,
	depth = -3,
	visible = 1,
	imageSet = 'logoWords',
	quad = 'BITS',
	iOfsX = 0,
	iOfsY = 0,
	name = "Bits",
	sfxthresh = 15,
	sfx = 'sfx/swish.ogg',
	turn = 2,
	menuLogic = Textbox.logoDisplay,
	initLogic = Textbox.waitMyTurn --logoDisplay

}

maps[2].templates[6] = {

	init = 1,
	cam = 1,
	x = 144,
	y = -33,
	ytgt = 38,
	yvel = 8*speedAdjust,
	depth = -2,
	visible = 1,
	imageSet = 'logoAnd',
	quad = 'AND',
	iOfsX = 0,
	iOfsY = 0,
	name = "And",
	sfxthresh = 8,
	sfx = 'sfx/slap.ogg',
	turn = 4,
	menuLogic = Textbox.logoDisplay,
	initLogic = Textbox.waitMyTurn --logoDisplay

}

maps[2].templates[7] = {

	init = 1,
	cam = 1,
	x = 325,
	y = 40,
	xtgt = 162,
	xvel = -16*speedAdjust,
	depth = -1,
	visible = 1,
	imageSet = 'logoWords',
	quad = 'BOTS',
--	bMode = 1,
--	bColor = 0xff202070,
--	alpha = .4,
	iOfsX = 0,
	iOfsY = 0,
	name = "Bots",
	sfxthresh = 15,
	sfx = 'sfx/swish.ogg',
	turn = 3,
	menuLogic = Textbox.logoDisplay,
	initLogic = Textbox.waitMyTurn --logoDisplay

}

maps[2].templates[8] = {

	init = 0,
	cam = 1,
	x = 160,
	y = 10,
	visible = 0,
	imageSet = 0,
	imageNum = 0,
	iOfsX = 0,
	iOfsY = 0,
	label = {
		visible = 1,
		text = "Instructions",
		x = -95,
		y = 0,
		yofs = 0,
		w = 190,
		h = 200,
		fontSet = 17,
		iMode = 0,
		hMode = 1,
		alpha = 1,
		color = {r=1,g=1,b=1,a=1},
		bkgColor = {r=.14,g=.23,b=.71,a=.38}, --0xff222260,
		bkgPadding = 8,
		brd = {r=1,g=1,b=1,a=1,w=1},
		brdWidth = 1
	},
	name = "InstrDisp",
	initLogic = Textbox.displayInstr --hoverYellow, --logoDisplay

}

maps[2].templates[9] = {

	init = 1,
	cam = 1,
	x = 160,
	y = 110,
	turn = 1,
	visible = 0,
	imageSet = 0,
	imageNum = 0,
	iOfsX = 0,
	iOfsY = 0,
	label = {
		visible = 1,
		text = "- MoikMellah presents -",
		x = -100,
		y = 0,
		w = 200,
		fontSet = 17,
		iMode = 0,
		hMode = 1,
		alpha = 0,
		color = {r=1,g=1,b=1,a=1}
--		bkgColor = {r=.14,g=.23,b=.71,a=.38}, --0xff222260,
--		bkgPadding = 5,
--		brd = {r=1,g=1,b=1,a=1,w=1},
--		brdWidth = 1
	},
	name = "MoikPresents",
	initLogic = Textbox.displayPresents

}

maps[2].cameras = {}

-- This was just for testing - please do NOT reenable it, as it messes with the title screen.
maps[2].cameras[1] = {

	init = 0,
	x = 50,
	y = 0,
	xOffset = 0,
	yOffset = 0,
	mapSet = 0,
	mapIndex = 0,
	imageSet = 3,
	visible = 0,
	initLogic = robotFollow

}

-- Starfield camera, (foreground)
maps[2].cameras[2] = {

	layerName = 'starFg',
	init = 2,
	x = 160,
	y = 120,
	xOffset = 0,
	yOffset = 0,
	xvel = 53,	--Scrolling speed
	xloop = 640+160,	--Point after which it loops back to farthest-left screen
	visBtn = INPUT_BA,	--Button to hide it (testing)
	mapSet = 0,
	mapIndex = 1,
	imageSet = 3,
	visible = 1,
	initLogic = Textbox.loopRight

}

-- Starfield camera, layer 3 (background)
maps[2].cameras[3] = {

	layerName = 'starBkg',
	init = 3,
	x = 160,
	y = 120,
	xOffset = 0,
	yOffset = 0,
	xvel = 29,	--Scrolling speed
	xloop = 640+160,	--Point after which it loops back to farthest-left screen
	visBtn = INPUT_BB,	--Button to hide it (testing)
	mapSet = 0,
	mapIndex = 1,
	imageSet = 3,
	visible = 1,
	initLogic = Textbox.loopRight --Camera.loopRight

}

maps[2].init = function()

	-- Sequence variable for intro animation
	turn = 1

	-- Other variables for menu handling
	inputOk = 1
	hideMenu = 0

	screenfx.r = 1
	screenfx.g = 1
	screenfx.b = 1
	screenfx.a = 1

	AudioLibrary:setMaster(1)

	AudioLibrary:play(menuMusic, true)

	-- Text files to import for menu popups; probably comment credits out, since we're not using it
	instrStr = fileToString("resource/text/instructions.txt")

	local aboutPath = 'resource/text/about.txt'

	if(love.system.getOS() == 'Android') then aboutPath = 'resource/text/about.android.txt' end

	aboutStr = fileToString(aboutPath)
	--creditStr = fileToString("resource/text/credits.txt")

	-- AudioLibrary:loadAudioFile('music/menu-skrjablin.ogg')

end
