BITS & BOTS

Orig. concept by Michael J. Miller

Built using the Love2D framework
- http://love2d.org

= ART =

Sprites/Backgrounds: MoikMellah
- License: CC0
- http://opengameart.org/content/bits-bots-art-pack  

Logo: Elton San Lorenzo
- License: CC-By-3.0
- http://eltonoverip.com

Bitmap Font: Clint Bellanger
- License: CC0
- http://opengameart.org/content/bitmap-font

NeoSans Font: usr_share
- License: CC0
- http://opengameart.org/node/12794/

= AUDIO =

..No audio yet.

= CODE =

Game code by moikmellah
- License: zlib
- http://moikmellah.org/blog/?p=19

Love2D Framework
- License: zlib
- http://love2d.org

love-android-sdl2
- Author: Martin Felis (fysx)
- License: zlib
- https://bitbucket.org/MartinFelis/love-android-sdl2

= LIBRARIES =

LuaJIT
- License: MIT
- http://luajit.org

SDL2
- License: zlib
- http://libsdl.org

DevIL
- License: LGPL
- http://openil.sourceforge.net

freetype-android
- License: Freetype
- http://freetype.org

JasPer
- License: JasPer License
- http://www.ece.uvic.ca/~frodo/jasper/

jpeg-9a
- License: JPEG
- http://ijg.org

lcms2
- License: MIT
- http://littlecms.com

libmng
- License: zlib
- http://libmng.com

libogg
- License: BSD
- http://xiph.org/ogg/

libpng
- License: libpng
- http://libpng.org

libvorbis
- License: BSD
- http://xiph.org/vorbis/

mpg123
- License: LGPL
- http://mpg123.org

openal-soft-android
- License: LGPL
- http://kcat.strangesoft.net/openal.html

physfs
- License: zlib
- http://icculus.org/physfs/

libtiff
- License: BSD
- http://libtiff.org

= OTHER =

Special thanks to:
- Dominique Miller
- Tim Zick
- James Hatchett
- Jelani Harris
- the OpenGameArt community
- the Love2D community

Thank you for playing!

[click to return]
