function love.conf(t)

	local ts = nil

        if(love._version_minor >= 9) then
                ts = t.window
        else
                ts = t.screen
        end

	t.identity = 'bitsnbots.love2d'
	t.title = "Bits N Bots"
	--ts.width = 640
	
	ts.width = 854
	ts.height = 480
	--ts.width = 0
	--ts.height = 0

	--t.window.width = 320
	--t.window.height = 240
	ts.fullscreen = false
	ts.resizable = true
	t.console = true

	t.modules.physics = false
	t.modules.thread = false

end
