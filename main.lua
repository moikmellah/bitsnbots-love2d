--[[
Copyright 2014 MoikMellah

This file is part of Bits & Bots.

Project SYPHA is free software: you can redistribute it and/or modify it under the terms
of the ZLIB License.

This code is provided strictly as-in and WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the ZLIB License for more details.

You should have received a copy of the ZLIB License along with
Bits & Bots. If not, see http://zlib.net/zlib_license.html
]]--

-- TODO: deprecate in favor of one Library script (using 'require' in individual files?)
require("utils.globals")
require("utils.utils")
require("utils.compat")
require("classes.AudioLibrary")
require("classes.InputLibrary")
--require("classes.Font")
require("classes.MapObject")
require("classes.LayerObject")
require("classes.GameObject")
require("classes.BkgObject")
require("classes.BkgResource")

require("classes.ImageLibrary")

require("classes.Buttons")
require("classes.Robots")
require("classes.Textbox")

require("classes.Map")

require("environments.testEnv")
require("environments.TitleScreen")

--[[

	From now on, this will be the function responsible for initializing the game (loading the first environment,
	initializing global vars and common resources, etc.).

]]--
function love.load(arg)

	globalCtx = love.graphics.newCanvas(vpWidth, vpHeight)
	bq = love.graphics.newQuad(0,0,vpWidth,vpHeight,vpWidth,vpHeight)
	love.graphics.setDefaultFilter('linear', 'nearest')

	local fontStr = ' !"#$%&'.."'"..'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~'
	--fontLib['large'] = love.graphics.newImageFont('resource/fonts/cb-bitmap-font.8x16.png', fontStr)
	fontLib['small'] = love.graphics.newImageFont('resource/fonts/NeoSans.png', fontStr, 1)
	fontLib['default'] = fontLib['small']

	AudioLibrary:loadAudioFile('sfx/slap.ogg')
	AudioLibrary:loadAudioFile('sfx/swish.ogg')
	AudioLibrary:loadAudioFile('sfx/thud.ogg')
	AudioLibrary:loadAudioFile('sfx/blip.ogg')
	AudioLibrary:loadAudioFile('sfx/click.ogg')
	AudioLibrary:loadAudioFile('sfx/warp.ogg')
	AudioLibrary:loadAudioFile('sfx/boop.ogg')

	love.graphics.setFont(fontLib['small'])
	activeFont = fontLib['small']

	--Font.cSetWindowLabel("Bits & Bots")
	--Font.cSetLineHeight(14)

	InputLibrary:init()
	
	-- Load background tilemap
--[[	BkgResource.cLoadTilemap("resource/maps/bitsnbots.80x60.map",80,60,0,0)
	BkgResource.cLoadTilemap("resource/maps/StarField.120x60.map",120,60,0,1)
]]--

	-- Load all sprite image files
--	BkgResource.cLoadImageFile("resource/images/LinearDragon.32x32.bmp", 0, 32, 32) -- Remove, unless we're using it for an intro
--[[
	BkgResource.cLoadImageFile("resource/images/botBlue.16x32.bmp", 1, 16, 32)
	BkgResource.cLoadImageFile("resource/images/botBlue.32x32.bmp", 1, 32, 32)

	BkgResource.cLoadImageFile("resource/images/botGreen.16x32.bmp", 10, 16, 32)
	BkgResource.cLoadImageFile("resource/images/botGreen.32x32.bmp", 10, 32, 32)

	BkgResource.cLoadImageFile("resource/images/botRed.16x32.bmp", 11, 16, 32)
	BkgResource.cLoadImageFile("resource/images/botRed.32x32.bmp", 11, 32, 32)

	BkgResource.cLoadImageFile("resource/images/botViolet.16x32.bmp", 12, 16, 32)
	BkgResource.cLoadImageFile("resource/images/botViolet.32x32.bmp", 12, 32, 32)
]]--
--[[
	BkgResource.cLoadImageFile("resource/images/goalRed.32x32.bmp", 13, 32, 32)
	BkgResource.cLoadImageFile("resource/images/goalBlue.32x32.bmp", 14, 32, 32)
	BkgResource.cLoadImageFile("resource/images/goalGreen.32x32.bmp", 15, 32, 32)
	BkgResource.cLoadImageFile("resource/images/goalViolet.32x32.bmp", 16, 32, 32)
]]--
--[[
	BkgResource.cLoadImageFile("resource/images/buttons.32x32.bmp", 4, 32, 32)
	BkgResource.cLoadImageFile("resource/images/eltonBits.64x32.bmp", 4, 64, 32)
	BkgResource.cLoadImageFile("resource/images/eltonAnd.32x32.bmp", 4, 32, 32)
]]--

	ImageLibrary:loadImageSet('logoWords')
	ImageLibrary:loadImageSet('logoAnd')

	ImageLibrary:loadImageSet('botBlue')
	ImageLibrary:loadImageSet('botGreen')
	ImageLibrary:loadImageSet('botRed')
	ImageLibrary:loadImageSet('botViolet')
	
	ImageLibrary:loadImageSet('goalBlue')
	ImageLibrary:loadImageSet('goalGreen')
	ImageLibrary:loadImageSet('goalRed')
	ImageLibrary:loadImageSet('goalViolet')

	ImageLibrary:loadImageSet('buttons')

	AudioLibrary:loadAudioFile(menuMusic, 1)	
	AudioLibrary:loadAudioFile(gameMusic, 1)	
	-- Load font (hopefully); format loadFontFromFile(filename, deffile, imageSet, fWidth, fHeight, maxImg);
	--Font.cLoadFont("resource/fonts/cb-bitmap-font.8x16.bmp", "resource/fonts/cb-bitmap-font.8x16.txt", 17, 8, 16)

	Map.init(2)

end

-- Main game loop; executes all object logic and rendering (CHANGETO: love.main)
function love.update(dt)

	-- Just drop this frame if we've dropped to 4fps.
	if(dt > .25) then
		dropct = dropct + 1
		if(dropct > 10) then
			print('Exiting due to poor FPS (<= 4).')
			love.event.quit()
		end
		return true
	else
		dropct = 0
	end

	--Input:update(dt)
	InputLibrary:updateInputStates(dt)
	-- Execute all active objects' logic
	for k,o in pairs(objs) do

		o.index = k

		if type(o.currentLogic) == "thread" then
			coroutine.resume(o.currentLogic, o, dt)
		else
			-- Something bad happened - reinit, I guess
			o.currentLogic = coroutine.create(o.initLogic)
		end

		-- Is there new logic to be had?
		if type(o.nextLogic) == "function" then
			-- Coro it up, and clear the nextLogic field
			o.currentLogic = coroutine.create(o.nextLogic)
			o.nextLogic = nil
		end

		if coroutine.status(o.currentLogic) == "dead" then
			--print("Logic has halted for obj #"..k.."("..o:toString().."). Removing.")
			o.killme = true
		end

	end

	-- Remove obsolete objects
	--	Note: This only removes them from objs.  Any other cleanup is the responsibility of AI.
	for ko = #objs,1,-1 do if objs[ko].killme == true then table.remove(objs,ko) end end

	-- Sort object list by depth, etc. (hopefully stable now - we'll see) 
	table.sort(objs,objDepthComp)

	-- Display all active objects (Images, Labels, and Primitives)
--[[	for k,o in pairs(objs) do

		o:display()

	end
]]--
	--if fadeColor ~= nil then BkgResource.cDisplayPrimitive(0,0,320,240,5,fadeColor) end

	if type(loadRequest) == "number" then
		Map.init(loadRequest)
		loadRequest = nil
	end

end

function love.draw()

	love.graphics.setColor(255,255,255,255)

	love.graphics.scale(1)

	--globalCtx:clear()

	love.graphics.setCanvas(globalCtx)
	love.graphics.clear()

	for k,o in pairs(objs) do
		o:display()

	end

	love.graphics.setCanvas()

	--love.graphics.scale(love.window.getHeight()/vpHeight)

	love.graphics.setDefaultFilter('linear', 'nearest')
	globalCtx:setFilter('linear', 'nearest')

	screenfx = screenfx or {r=1,g=1,b=1,a=1}
	love.graphics.setColor((screenfx.r or 1) * 255, (screenfx.g or 1) * 255, (screenfx.b or 1) * 255, (screenfx.a or 1) * 255)

	local ww,wh
	ww,wh = love.graphics.getWidth(),love.graphics.getHeight()
	--love.graphics.draw(globalCtx, bq, 160, 0, 0, love.window.getWidth() / vpWidth, love.window.getHeight() / vpHeight, 160, 0)
	love.graphics.draw(globalCtx, bq, ww/2, wh/2, 0, ww/vpWidth * scrMirror.x, wh/vpHeight * scrMirror.y, vpWidth/2, vpHeight/2)

	love.graphics.setColor(255,255,255,255)

	--love.graphics.print('FPS: '..love.timer.getFPS()..'; '..ww..'x'..wh, 2, 2)
	--love.graphics.print('FPS: '..love.timer.getFPS(), 2, 2)

--	if fadeColor ~= nil then
--		love.graphics.drawRect()
--	end

end
